describe('testes do modulo diario.criarTabelasService',function(){
	var inserirAlunosService;
	var query;
	beforeEach(module('diario.criarTabelasService'));
	
	beforeEach(inject(function(_criarTabelasService_){
		inserirAlunosService =_criarTabelasService_;
	}));
	
	it(" Deve conter uma constante TABELA_ALUNO_MATRICULA",function(){
		expect(inserirAlunosService.TABELA_ALUNO_MATRICULA).toEqual(
				"create table if not exists aluno_matricula (\n" + 
				"  id_aluno_matricula integer,\n" + 
				"  id_unidade_funcional integer,\n" + 
				"  id_ano_semestre integer,\n" + 
				"  id_serie integer,\n" + 
				"  data_inclusao numeric,\n" + 
				"  data_matricula numeric,\n" + 
				"  id_curso integer,\n" + 
				"  id_cidadao integer,\n" + 
				"  id_aluno_matr_status Integer,\n" + 
				"\n" + 
				"  foreign key (id_unidade_funcional) references unidade_funcional(id_unidade_funcional),\n" + 
				"  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre),\n" + 
				"  foreign key (id_serie) references serie(id_serie),\n" + 
				"  foreign key (id_curso) references curso(id_curso),\n" + 
				"  foreign key (id_cidadao) references cidadao(id_cidadao),\n" + 
				"  foreign key (id_aluno_matr_status) references aluno_matr_status(id_aluno_matr_status) \n" + 
				")");
	});
	
	it("deve conter uma constante tabela criarTabelaAlunoMatrStatusQuery",function(){
		expect(inserirAlunosService.TABELA_ALUNO_MATR_STATUS).toEqual(
				"create table if not exists aluno_matr_status(\n" + 
				"  id_aluno_matr_status integer primary key,\n" + 
				"  descricao text,\n" + 
				"  flg_status_ativo numeric\n" + 
				")");
	});
	
});