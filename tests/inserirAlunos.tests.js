describe('testes do modulo diario.inserirAlunosService',function(){
	var inserirAlunosService;
	beforeEach(module('diario.inserirAlunosService'));
	
	beforeEach(inject(function(_inserirAlunosService_){
		inserirAlunosService = _inserirAlunosService_;
	}));
	
	it(" Deve conter uma funcao  prepararQueryEValorArgumentos",function(){
		expect(inserirAlunosService.prepararQueryEValorArgumentos).toBeDefined();
	});
});