angular.module("diario.esqueceuSenhaController",[])
	.controller("esqueceuSenhaController",["$state", "$ionicPopup",function($state , $ionicPopup){
		var ctx = this;
		
		ctx.voltar = function(){
			$state.go("login");
		};
		
		ctx.enviar = function(){
			$ionicPopup.alert({
				title:"Email enviado com sucesso.",
				template:"Voce vai receber instrucoes no email cadastrado."
			})
		};
	}]);