angular.module('diario.roteamento',[])
	.config(function($urlRouterProvider, $stateProvider){
		$stateProvider
			.state('login',{
				url:"/login",
				templateUrl:"js/login/login.view.html"
			})
			
			.state('listarUnidade',{
				url:"/listarUnidades/{idUsuario}",
				templateUrl:"js/listarunidade/listarUnidade.view.html"
			})
			
			.state('listarTurma',{
				url:"/listarTurmas/unidade/{idUnidade}",
				templateUrl:"js/listarturma/listarTurma.view.html"
			})
			
			.state('listarComponenteCurricular',{
				url:"/listarComponentes/unidade/{idUnidade}/turma/{idTurma}",
				templateUrl:"js/listarcomponentecurricular/listarComponenteCurricular.view.html"
			})
			
			.state('listarModulo',{
				url:"/listarModulos/",
				templateUrl:"js/listarmodulo/listarModulo.view.html"
			})
			
			.state('listarAula',{
				url:'/listarAula/unidade/{idUnidade}/turma/{idTurma}/componente/{idComponente}',
				templateUrl:"js/listaraula/listarAula.view.html"
			})
			
			.state('lancarAlunoFrequencia',{
				url:"/lancarAlunoFrequencia/unidade/{idUnidade}/turma/{idTurma}/componente/{idComponente}/data/{data}",
				templateUrl:"js/lancaralunofrequencia/lancarAlunoFrequencia.view.html"
			})
			
			.state('lancarFrequencia',{
				url:'/lancarFrequencia/unidade/{idUnidade}/turma/{idTurma}/componente/{idComponente}/data/{data}/aluno/{idAluno}',
				templateUrl:"js/lancarfrequencia/lancarFrequencia.view.html"
			})
			
			.state('informacoesAluno',{
				url:'/informacoesAluno/unidade/{idUnidade}/turma/{idTurma}/componente/{idComponente}/data/{data}/aluno/{idAluno}',
				templateUrl:"js/informacoesaluno/informacoesAluno.view.html",
			})

//          a funcionalidade de recuperar senha será adiada para uma proxima versão 			
//			.state('esqueceuSenha',{
//				url:"/esqueceuSenha",
//				templateUrl:"js/esqueceusenha/esqueceuSenha.view.html"
//			})
		$urlRouterProvider.otherwise("");
	});