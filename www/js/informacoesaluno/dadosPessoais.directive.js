(function(){
	
	angular.module('diario.dadosPessoaisDirective',[])
		.directive('dadosPessoaisDirective',dadosPessoaisDirective)
	
	dadosPessoaisDirective.$inject = ['informacoesAlunoService', 'conversorService']
	function dadosPessoaisDirective(informacoesAlunoService, conversorService){
		return{
			restrict:"E",
			templateUrl:"js/informacoesaluno/templates/dadosPessoais.directive.html",
			scope:{
				idAlunoMatricula:"@"
			},
			
			link:function($scope, $element, $attr){
				informacoesAlunoService.recuperarInformacoesPessoaisAluno($scope.idAlunoMatricula)
				.then(function(response){
          var info = conversorService.toObject(response.rows);
          info.sexo = (info.sexo == 'F') ? 'Feminino' : 'Masculino';
					return $scope.informacoes = info; 
				});
			}
		
		};
	}
})();
