(function(){
	angular.module('diario.dadosMatriculaDirective',[])
		.directive('dadosMatriculaDirective',dadosMatriculaDirective);
	
		                                     
	dadosMatriculaDirective.$inject = ['informacoesAlunoService', 'conversorService']	                                     
	
	function dadosMatriculaDirective(informacoesAlunoService, conversorService){
		return{
			restrict:"E",
			templateUrl:"js/informacoesaluno/templates/dadosMatricula.directive.html",
			scope:{
				idAlunoMatricula:"@"
			},
			
			link:function($scope, $element, $attr){
				informacoesAlunoService.recuperarInformacoesMatriculaAluno($scope.idAlunoMatricula)
				.then(function(response){
					return $scope.informacoes = conversorService.toObject(response.rows);
				});
			}
		} ;
	}
})();