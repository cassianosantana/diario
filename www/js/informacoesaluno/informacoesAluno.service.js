(function(){
	'use strict';
	angular.module('diario.informacoesAlunoService',[])
		.factory('informacoesAlunoService', informacoesAlunoService);
		
	informacoesAlunoService.$inject = ['conexaoService'];
	
	function informacoesAlunoService(conexaoService){
		
		var service = {
				constants:constants,
				recuperarInformacoesMatriculaAluno: recuperarInformacoesMatriculaAluno,
				recuperarInformacoesPessoaisAluno:recuperarInformacoesPessoaisAluno,
		};
		return service;
		
		function recuperarInformacoesMatriculaAluno(idAlunoMatricula){
			return conexaoService.executar(informacoesMatriculaQuery,[idAlunoMatricula] )
		}
		
		function recuperarInformacoesPessoaisAluno(idAlunoMatricula){
			return conexaoService.executar(informacoesPessoaisQuery,[idAlunoMatricula])
		}
		
	}
	var constants = {
			DADOS_MATRICULA:"DADOS_MATRICULA",
			DADOS_PESSOAIS:"DADOS_PESSOAIS",
			DADOS_FREQUENCIA:"DADOS_FREQUENCIA"
		};
	
	var informacoesMatriculaQuery = 
		"select \r\n" +
		"a.codigo as matricula, " + 
		"am.data_matricula,\r\n" + 
		" case when asem.semestre is not null then asem.ano ||' / '||asem.semestre else asem.ano end as ano ,\r\n" + 
		"c.descricao as curso, \r\n" + 
		"s.sigla as serie, \r\n" + 
		"t.nome as turma, \r\n" + 
		"tu.sigla as turno,\r\n" + 
		"s.idade_distorcao\r\n" + 
		"from aluno_matricula am \r\n" + 
		"inner join aluno_matr_turma amt on am.id_aluno_matricula = amt.id_aluno_matricula\r\n" +
		"inner join aluno a on a.id_cidadao = am.id_cidadao \r\n" + 
		"inner join turma t on amt.id_turma = t.id_turma\r\n" + 
		"inner join turno tu on tu.id_turno = t.id_turno \r\n" + 
		"inner join ano_semestre asem on asem.id_ano_semestre = t.id_ano_semestre\r\n" + 
		"inner join serie s on s.id_serie = am.id_serie\r\n" + 
		"inner join curso c on c.id_curso = am.id_curso\r\n" + 
		"where  am.id_aluno_matricula = ?";
	
	var informacoesPessoaisQuery = 
		"select \r\n" + 
		"cid.nascimento_data,\r\n" + 
		"cid.sexo,\r\n" + 
		"cid.flg_necessidade_especial\r\n" + 
		"from aluno_matricula am \r\n" + 
		"inner join cidadao cid on cid.id_cidadao = am.id_cidadao \r\n" + 
		"where am.id_aluno_matricula = ?";
	
	})();