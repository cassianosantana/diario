(function(){
	'use strict';
	
	angular.module('diario.informacoesAlunoController',[])
		.controller('informacoesAlunoController', informacoesAluno);
	
	informacoesAluno.$inject = ['$state',
                                '$ionicViewSwitcher',
                                'lancarAlunoFrequenciaService',
                                'informacoesAlunoService',
                                '$ionicSideMenuDelegate'
                                ];	                                          
	function informacoesAluno($state, $ionicViewSwitcher, lancarAlunoFrequenciaService, 
			informacoesAlunoService, $ionicSideMenuDelegate){
		var ctx = this;
		
		// binds do modelo informacoes aluno na view informacoes aluno
		//manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.id_aluno_matricula = $state.params.idAluno;
		ctx.atual = informacoesAlunoService.constants.DADOS_MATRICULA;
		ctx.DADOS_FREQUENCIA = informacoesAlunoService.constants.DADOS_FREQUENCIA;
		ctx.DADOS_PESSOAIS = informacoesAlunoService.constants.DADOS_PESSOAIS;
		ctx.DADOS_MATRICULA = informacoesAlunoService.constants.DADOS_MATRICULA;
		ctx.isMenuAberto = isMenuAberto;
		ctx.isMostrarMatricula = true;
		ctx.mostrar = mostrar;
		ctx.voltar = voltar;
		
		//implementacoes dos binds 
		// manter ordem alfabetica
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}
		
		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}
		
		function mostrar(directive){
			ctx.isMostrarMatricula = (directive === informacoesAlunoService.constants.DADOS_MATRICULA);
			ctx.isMostrarPessoais = (directive === informacoesAlunoService.constants.DADOS_PESSOAIS);
			ctx.isMostrarFrequencia = (directive === informacoesAlunoService.constants.DADOS_FREQUENCIA); 
		}
		
		function recuperarInformacoesAluno(idAlunoMatricula){
			return lancarAlunoFrequenciaService.getAluno(idAlunoMatricula)
			  .then(function(data){
				return ctx.aluno = data
			 });
		}
		
		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		}
	}
})();