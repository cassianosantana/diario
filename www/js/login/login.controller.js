(function() {
  angular.module('diario.loginController', []).controller('loginController', loginController);

  // dependencias de loginController
  loginController.$inject = [ '$state', 'tecladoService', '$ionicPopup', 'loginService',
      'conexaoRedeService', '$cordovaKeyboard', 'conexaoService', 'menuDireitoService',
      '$q', '$ionicLoading', '$http'];

  function loginController($state, tecladoService, $ionicPopup, loginService, conexaoRedeService,
      $cordovaKeyboard, conexaoService, menuDireitoService, $q, $ionicLoading, 
      $http, saltoUnidadeService, saltoModuloService, saltoService) {
    var ctx = this;

    /* implementacoes manter ordem alfabética */
    // verifica se o usuario é valido
    // e se for cria as tabelas da aplicação no primeiro login
    this.autenticar = function(usuario) {
      usuario = usuario || {};
      // if(!conexaoRedeService.temConexao()){
      // mostrarPopup("Sem conexão", "Para realizar o login inicial neste aplicativo é necessário
      // que se tenha conexão com a internet");
      // return;
      // }
      this.bloquearTela();
      return loginService.validarUsuario(usuario).then(function(response) {
        try {
          $cordovaKeyboard.close();
        } catch (err) {}
        ctx.desbloquearTela();
        if (response.ok) {
      	  ctx.listarModulos();
        } else {
          ctx.mostrarPopup("Erro de Autenticação", response.mensagem);
        }
      });
    };
    
    this.bloquearTela = function(){
    	$ionicLoading.show({
    	      templateUrl: "js/login/carregando.html"
    	    });
    };

    this.calcularSaltoListarModulo = function(unidade){
    	if(unidade.numero_unidades === 1){
  		  saltoModuloService.setProximaTelaModulo(saltoService.LISTAR_TURMA);  
      } else {
  		  saltoModuloService.setProximaTelaModulo(LISTAR_UNIDADE);
      }
    	$state.params.idUnidade = unidade.id_unidade;
    };
    
    this.desbloquearTela = function(){
    	 $ionicLoading.hide();
    };

    this.esqueceuSenha = function() {
      $state.go("esqueceuSenha");
      return;
    };

    this.listarModulos = function() {
      $state.go("listarModulo");
      return;
    };

    this.mostrarPopup = function(titulo, texto) {
      $ionicPopup.alert({
        title : titulo,
        template : texto
      });
      return;
    };

    this.senhaPressionado = function($event, usuario) {
      if (!tecladoService.isTeclaEnterPressionada($event.keyCode)) {
        return;
      }
      this.autenticar(usuario);
    };

    this.usuarioPressionado = function($event) {
      if (!tecladoService.isTeclaEnterPressionada($event.keyCode)) {
        return;
      }
      this.focalizarSenha = true;
    };

  }
})();
