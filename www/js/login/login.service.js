(function(){
	angular.module('diario.loginService',[]).factory('loginService',loginService);
	
    loginService.$inject = [
      'conexaoService', 
      '$http', 
      '$ionicLoading', 
      'criarTabelasService', 
      '$q', 
      'popularTabelasService', 
      'conversorService'];
	
	function loginService(
	    conexaoService, 
      $http, 
      $ionicLoading, 
      criarTabelasService, 
      $q, 
      popularTabelasService, 
      conversorService) {
		
		var service = {};

    service.usuario = null;
		
		service.validarUsuario = function(usuario){
      var deferred = $q.defer();

      if(usuario.login == null || usuario.login.trim() == '' ||
        usuario.senha == null || usuario.senha.trim() == '') {
        deferred.resolve({
          "ok": false,
          "mensagem": "Informe um login e uma senha."
        });
        return deferred.promise;
      }

      var data = null;

      var promise = null;
      if(DEMONSTRACAO) {
        if(["65758", "66266"].indexOf(usuario.login) == -1) {
          deferred.resolve({
            "ok": false,
            "mensagem": "Usuário inválido."
          });
          return deferred.promise;
        } else {
          promise = $q.when({data:{nomeUsuario: "Usuário " + usuario.login, idUsuario: usuario.login, token: "0b456431-6cb3-4584-b150-2fe68edac0b3"}});
        }
      } else {
        // Solicitar autenticacao no backend
        promise = $http.post(URL_BACKEND + "/ws/seguranca/autenticar", {
          "idDispositivo": device.uuid,
          "modeloDispositivo": device.model,
          "plataformaDispositivo": device.platform,
          "versaoDispositivo": device.version,
          "fabricanteDispositivo": device.manufacturer,
          "login": usuario.login,
          "senha": usuario.senha
        });
      }
      promise.then(function(response){
        // Armazenar usuario
        localStorage["usuario"] = angular.toJson(response.data);

        // Fazer login no sistema
        service.login().then(function() {
          // Retornar sucesso
          deferred.resolve({
            "ok": true
          });
        });
      }, function(response) {
        // Caso erro, responder com mensagem de erro recebida
        deferred.resolve({
          "ok": false,
          "mensagem": (response.status == 401) ? response.data : "Erro de conexão com o servidor."
        });
      });

      return deferred.promise;
    };

		service.login = function() {
      var deferred = $q.defer();
      if(localStorage['usuario'] == null) {
        // Nao ha usuario logado
        deferred.resolve(false);
      } else {
        service.usuario = angular.fromJson(localStorage['usuario']);
        conexaoService.recuperarBanco(service.usuario.idUsuario);

        criarTabelasService.criarTabelas().then(function(criar) {
          // TODO: Com a sincronizacao funcionando, isto aqui deve acabar
          if(criar) {
            popularTabelasService.popularTabelas(service.usuario.idUsuario).then(function() {
              deferred.resolve(true);
            });
          } else {
            deferred.resolve(true);
          }
        });
      }
      return deferred.promise;
    };

    service.logoff = function() {
      localStorage.removeItem("usuario");
    };

    service.iniciarSessao = function() {
      return $http.post(URL_BACKEND + "/ws/seguranca/iniciar_sessao", {
        "token": localStorage["usuario"]["token"]
      });
    };

		return service;
	}
})();
