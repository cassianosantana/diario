(function(){
	angular.module("diario.listarUnidadeController",[])
	.controller("listarUnidadeController",listarUnidadeController);
	
	listarUnidadeController.$inject = ["$state",
	                                   "$ionicViewSwitcher",
	                                   "$ionicSideMenuDelegate",
	                                   "listarUnidadeService",
	                                   "menuDireitoService",
	                                   "conversorService",
	                                   "$ionicScrollDelegate",
	                                   "saltoService"
	                                   ];
	
	function listarUnidadeController(
			$state, 
			$ionicViewSwitcher, 
			$ionicSideMenuDelegate,
			listarUnidadeService,
			menuDireitoService,
			conversorService,
			$ionicScrollDelegate,
			saltoService){
		
		var ctx = this;

		/// binds ao modelo de listar unidade
		/// manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.backView = saltoService.getBackViewUnidade();
		ctx.isMenuAberto = isMenuAberto;
		ctx.limparBusca = limparBusca;
		ctx.recuperarTurmas = recuperarTurmas;
		ctx.unidades = listarUnidades();
		ctx.voltar = voltar;
		
		// imlementacoes dos binds do modelo 
		//manter ordem alfabetica
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		};
		
		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		};
		
		function limparBusca(){
			return ctx.txtBusca = "";
			
		}
		
		function listarUnidades(){
			 listarUnidadeService.getUnidades()
			 .then(function(response){
				 ctx.unidades = conversorService.toArray(response.rows)
			 })
		};
		
		function prepararNavegacao(){
			var isTurmaRegencia = null;
			return saltarTurmaService.numeroTurmasUsuario(response.id_unidade_funcional)
			.then(function(response){
				if(!response){
					return;
				}
				return saltarTurmaService.isTurmaNecessario(response.numero_turma);
			})
			.then(function(isNecessarioListarTurma){
				saltoService.setMostrarTurma(isNecessarioListarTurma);	
				if(isNecessarioListarTurma){
					ctx.proximaView = "listarTurma";
					return;
				}else{
					return saltarTurmaService.recuperarTurmaUnica($state.params.idUnidade);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				if(response.id_turma){
					$state.params.idTurma = response.id_turma;
					menuDireitoService.setTurmaAtual(response.nome);
					return listarTurmaService.isTurmaRegenciaClasse(response.id_turma);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				if(response === "true"){
					ctx.proximaView = "listarAula";
					menuDireitoService.setComponenteAtual("Regência de classe");
				}
				return saltarComponenteCurricularService.numeroComponentesUsuario($state.params.idUnidade, $state.params.idTurma, response);
			})
			.then(function(response){
				if(!response){
					return;
				}
				isTurmaRegencia = response.numero_componente == 0;
				return saltarComponenteCurricularService.islistarComponenteNecessario(response.numero_componente);
			})
			.then(function(isNecessarioListarComponente){
				saltoService.setMostrarComponente(isNecessarioListarComponente);
				if(isNecessarioListarComponente === true){
					return ctx.proximaView = "listarComponenteCurricular";
				}
				if(isTurmaRegencia != null && isTurmaRegencia == false &&  isNecessarioListarComponente == false ){
					return saltarComponenteCurricularService.recuperarComponenteUnico($state.params.idUnidade, $state.params.idTurma);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				menuDireitoService.setComponenteAtual(response.descricao);
				$state.params.idComponente = response.id_componente_curricular;
				return;
			});
		}
		
		function recuperarTurmas(idUnidadeFuncional){
			listarUnidadeService.getUnidade(idUnidadeFuncional)
			.then(function(response){
				menuDireitoService.setUnidadeAtual(conversorService.toObject(response.rows).nome);
				$ionicViewSwitcher.nextDirection('forward');
				return prepararNavegacao()
			})
			.then(function(response){
				$state.go(ctx.proximaView,$state.params);
			})
		};
		
		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state);
		};
	}
})();