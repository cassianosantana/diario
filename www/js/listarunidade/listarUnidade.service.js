(function(){
	angular.module('diario.listarUnidadeService',[])
	.factory('listarUnidadeService',listarUnidadeService);
	
	listarUnidadeService.$inject = ['conexaoService','$q'];
	
	function listarUnidadeService(conexaoService, $q){
		
		// revealing module pattern  vc cosegue enchergar todos os metodos dos services 
		// sem dificuldade 
		/// implementacoes estao abaixo 
		/// vc vai na desejada
		var service = {
				getUnidade:getUnidade,
				getUnidades:getUnidades
		};
		
		return service;
		// implementacoes 
		//  dos membros da factory 
		// manter ordem algabética
		
		/// recupera a unidade selecionada
		function getUnidade(idUnidade){
			return conexaoService.executar(querySelecionarUnidade,[idUnidade])
		}
		// recupera todas as unidades do usuario
		function getUnidades(){
			return conexaoService.executar(_queryListar);
		}
		
		
	}
	
	
	var _queryListar  = 
		"select \n" + 
		"id_unidade_funcional,\n" + 
		"nome,\n" + 
		"codigo\n" + 
		"from unidade_funcional order by nome ";
	
	// unidades para mock up remover quando existir acesso real a dados
	
	var querySelecionarUnidade =
		"select \n" + 
		"uf.nome \n" + 
		"from \n" + 
		"unidade_funcional uf \n" + 
		"where uf.id_unidade_funcional = ?\n";
	
})();