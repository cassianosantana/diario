(function(){
	angular.module('diario.saltarUnidadeService',[])
		.factory('saltarUnidadeService',saltarUnidadeService);
	
	saltarUnidadeService.$inject = ['conexaoService', 'conversorService']
	
	function saltarUnidadeService(conexaoService, conversorService){
		var service = {
				numeroUnidadesUsuario:numeroUnidadesUsuario,
				isListarUnidadesnecessario:isListarUnidadesnecessario,
				recuperarUnidadeUnica:recuperarUnidadeUnica
					
		}
		return service;
		
		function numeroUnidadesUsuario(){
			return conexaoService.executar(contarUnidadeUnicaQuery,[])
			.then(function(response){
				return conversorService.toObject(response.rows)
			})
		}
		
		function isListarUnidadesnecessario(numeroUnidadeUsuario){
			if(numeroUnidadeUsuario == 1){
				return false;
			}else{
				return true;
			}
		}
		
		function recuperarUnidadeUnica(){
			return conexaoService.executar(recuperarIdUnidadeUnicaQuery,[])
			.then(function(response){
				return conversorService.toObject(response.rows)
			})
		}
	}
	
	
	
	var contarUnidadeUnicaQuery =
		"select \n" + 
		"count (uf.id_unidade_funcional) as numero_unidade\n" + 
		"from unidade_funcional uf"
	
	var recuperarIdUnidadeUnicaQuery = 
		"select \n" + 
		"id_unidade_funcional, \n" +
		" nome \n" + 
		"from \n" + 
		"unidade_funcional"
		
})();