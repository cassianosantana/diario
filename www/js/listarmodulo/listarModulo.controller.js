(function(){
	angular.module('diario.listarModuloController',[]).controller('listarModuloController',listarModuloController);
	
	listarModuloController.$inject = ['$state',
	                                  '$ionicViewSwitcher',
                                      '$ionicSideMenuDelegate',
                                      'loginService',
                                      'listarUnidadeService',
                                      'listarTurmaService',
                                      'componenteCurricularService',
                                      'menuDireitoService',
                                      'saltarUnidadeService',
                                      'saltarTurmaService',
                                      'saltarComponenteCurricularService',
                                      'saltoService'];
	
	function listarModuloController($state, $ionicViewSwitcher,$ionicSideMenuDelegate,
	    loginService, listarUnidadeService,listarTurmaService,componenteCurricularService, 
	    menuDireitoService, saltarUnidadeService, saltarTurmaService, 
	    saltarComponenteCurricularService, saltoService){
		
		var ctx = this;
		
		ctx.limparDadosMenuDireito = function(){
			menuDireitoService.setUnidadeAtual(null);
			menuDireitoService.setTurmaAtual(null);
			menuDireitoService.setDataAtual(null);
			menuDireitoService.setComponenteAtual(null);
		}
		
		ctx.limparDadosMenuDireito();

		this.nomeUsuario = function() {
			return loginService.usuario.nomeUsuario;
		};
		
		// implementacoes presentes no Controller 
		// ordem alfabetica
		this.abrirMenu = function(){
			$ionicSideMenuDelegate.toggleRight();
		};
	
		this.isMenuAberto = function(){
			return $ionicSideMenuDelegate.isOpenRight();
		};
		
		this.lancarFrequencia = function(){
			return ctx.prepararNavegacao()
			.then(function(response){
				$ionicViewSwitcher.nextDirection('forward');
				$state.go(ctx.proximaView,$state.params);
			});
		};
		
		
		
		this.prepararNavegacao = function(){
			var isTurmaRegencia = null;
			return saltarUnidadeService.numeroUnidadesUsuario()
			.then(function(response){
				return saltarUnidadeService.isListarUnidadesnecessario(response.numero_unidade);
			})
			.then(function(isNecessarioListarUnidade){
				saltoService.setMostrarUnidade(isNecessarioListarUnidade);
				if(isNecessarioListarUnidade){
					 ctx.proximaView ="listarUnidade";
					 return;
				} else {
					return saltarUnidadeService.recuperarUnidadeUnica();
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				if(response.id_unidade_funcional){
					$state.params.idUnidade = response.id_unidade_funcional;
					menuDireitoService.setUnidadeAtual(response.nome);
				}
				return saltarTurmaService.numeroTurmasUsuario(response.id_unidade_funcional);
			})
			.then(function(response){
				if(!response){
					return;
				}
				return saltarTurmaService.isTurmaNecessario(response.numero_turma);
			})
			.then(function(isNecessarioListarTurma){
				saltoService.setMostrarTurma(isNecessarioListarTurma);	
				if(isNecessarioListarTurma){
					ctx.proximaView = "listarTurma";
					return;
				}else{
					return saltarTurmaService.recuperarTurmaUnica($state.params.idUnidade);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				if(response.id_turma){
					$state.params.idTurma = response.id_turma;
					menuDireitoService.setTurmaAtual(response.nome);
					return listarTurmaService.isTurmaRegenciaClasse(response.id_turma);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				if(response === "true"){
					ctx.proximaView = "listarAula";
					menuDireitoService.setComponenteAtual("Regência de classe");
				}
				return saltarComponenteCurricularService.numeroComponentesUsuario($state.params.idUnidade, $state.params.idTurma, response);
			})
			.then(function(response){
				if(!response){
					return;
				}
				isTurmaRegencia = response.numero_componente == 0;
				return saltarComponenteCurricularService.islistarComponenteNecessario(response.numero_componente);
			})
			.then(function(isNecessarioListarComponente){
				saltoService.setMostrarComponente(isNecessarioListarComponente);
				if(isNecessarioListarComponente === true){
					return ctx.proximaView = "listarComponenteCurricular";
				}
				if(isTurmaRegencia != null && isTurmaRegencia == false &&  isNecessarioListarComponente == false ){
					return saltarComponenteCurricularService.recuperarComponenteUnico($state.params.idUnidade, $state.params.idTurma);
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				menuDireitoService.setComponenteAtual(response.descricao);
				$state.params.idComponente = response.id_componente_curricular;
				return;
			});
		};
			
		this.voltar = function(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		};
	}
})();
