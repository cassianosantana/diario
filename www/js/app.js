var URL_BACKEND = "http://localhost:8080";
var DEMONSTRACAO = true;

var diario = angular.module(
    'diario',
    [ 'ionic',
      'ngCordova',
      'diario.roteamento',
      'diario.loginController',
      'diario.esqueceuSenhaController',
      'diario.listarUnidadeController',
      'diario.listarTurmaController',
      'diario.listarComponenteCurricularController',
      'diario.listarModuloController',
      'diario.listarAulaController',
      'diario.lancarAlunoFrequenciaController',
      'diario.lancarFrequenciaController',
      'diario.horarioFaltaController',
      'diario.menuDireitoController',
      'diario.informacoesAlunoController',
      'diario.menuDireitoService',
      'diario.listarUnidadeService',
      'diario.listarTurmaService',
      'diario.listarcomponenteCurricularService',
      'diario.listarAulaService',
      'diario.datePickerService',
      'diario.lancarAlunoFrequenciaService',
      'diario.horarioFaltaService',
      'diario.informacoesAlunoService',
      'diario.conexaoRedeService',
      'diario.criarTabelasService',
      'diario.conexaoService',
      'diario.conversorService',
      'diario.conversorService',
      'diario.tecladoService',
      'diario.loginService',
      'diario.popularTabelasService',
      'diario.formatadorService',
      'diario.escolaCalendarioService',
      'diario.cursoCalendarioService',
      'diario.calendarioService',
      'diario.calendarioDataService',
      'diario.quadroHorarioService',
      'diario.horarioQuadroService',
      'diario.turmaSerieService',
      'diario.faltaAlunoDisciplinaDiarioService',
      'diario.turmaMultiSeriadaService',
      'diario.turmaMultiEtapaService',
      'diario.atualizarFaltaAlunoDisciplinaDiarioService',
      'diario.horarioTurmaService',
      'diario.atualizarFaltaAlunoGlobalizadaDiarioService',
      'diario.faltaAlunoGlobalizadaDiarioService',
      'diario.saltarUnidadeService',
      'diario.saltarTurmaService',
      'diario.saltarComponenteCurricularService',
      'diario.saltoService',
      'diario.registroFaltaDisciplinaDiarioService',
      'diario.registroFaltaGlobalDiarioService',
      'diario.atualizarRegistroFaltaDisciplinaDiarioService',
      'diario.atualizarRegistroFaltaGlobalDiarioService',
      'diario.lancarFrequenciaDirective',
      'diario.popupConcluidoDirective',
      'diario.dadosMatriculaDirective',
      'diario.dadosPessoaisDirective',
      'diario.dadosFrequenciaDirective',
      'diario.sincronizacaoService',
      'diario.datePickerDirective',
      'diario.focusDirective' ]).run(
    function($ionicPlatform, conexaoService, criarTabelasService, $state, menuDireitoService, loginService) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the
        // keyboard for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          StatusBar.styleDefault();
        }
        if (!window.device) {
          if (!localStorage.getItem("uuid")) {
            var guid = (function() {
              function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
              }
              return function() {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
              };
            })();
            localStorage.setItem("uuid", guid());
          }
          window.device = {
            available : true,
            cordova : "4.0.0",
            isVirtual : undefined,
            manufacturer : "unknown",
            model : "Chrome",
            platform : "browser",
            serial : "unknown",
            uuid : localStorage.getItem("uuid"),
            version : "unknown"
          };
        }

        loginService.login().then(function(response) {
          if(response) {
            $state.go("listarModulo");
          } else {
            $state.go("login");
          }
        });

      });
    });

diario.config([ '$httpProvider', function($httpProvider) {
  $httpProvider.defaults.withCredentials = true;
} ]);
