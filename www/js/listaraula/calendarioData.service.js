(function(){
	angular.module('diario.calendarioDataService',[])
		.factory('calendarioDataService',calendarioDataService);
	
	calendarioDataService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];
	
	function calendarioDataService(conexaoService, conversorService, formatadorService){
		var service = {
				isDataEspecial:isDataEspecial
		};
		
		return service;
		
		function isDataEspecial(idCalendario, data){
			return conexaoService.executar(calendarioDataQuery,[idCalendario, formatadorService.formatatarDataSql(data)])
			.then(function(response){
				var motivoEspecial =  conversorService.toArray(response.rows);
				if(motivoEspecial.length != 0){
					throw new Error("Data inválida, "+
							(motivoEspecial[0].dia_nao_letivo?motivoEspecial[0].dia_nao_letivo:"")+
							" "+(motivoEspecial[0].dia_letivo_extra?motivoEspecial[0].dia_letivo_extra:"") +
							" "+(motivoEspecial[0].evento?motivoEspecial[0].evento:""));
				}else{
					return false;
				}
			});
		}
	}
	
	var calendarioDataQuery = 
		"select  \r\n" + 
		"cd.id_calendario,\r\n" + 
		"mdnl.descricao as dia_nao_letivo,\r\n" + 
		"mdle.descricao as dia_letivo_extra,\r\n" + 
		"me.descricao as evento,\r\n" + 
		"cd.data\r\n" + 
		"from calendario_data cd\r\n" + 
		"	left join motivo_dia_nao_letivo mdnl on mdnl.id_motivo_dia_nao_letivo = cd.id_motivo_dia_nao_letivo\r\n" + 
		"	left join motivo_dia_letivo_extra mdle on mdle.id_motivo_dia_letivo_extra = cd.id_motivo_dia_letivo_extra \r\n" + 
		"	left join motivo_evento me on me.id_motivo_evento = cd.id_motivo_evento\r\n" + 
		"where \r\n" + 
		"	id_calendario = ?\r\n" + 
		"	and date(cd.data) = ?	"
})();