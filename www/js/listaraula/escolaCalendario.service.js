(function(){
	angular.module('diario.escolaCalendarioService',[])
		.factory('escolaCalendarioService',escolaCalendarioService);
	
	escolaCalendarioService.$inject =['conexaoService', 'conversorService'];
	
	function escolaCalendarioService(conexaoService, conversorService){
		var service = {
				getIdEscolaCalendario:getIdEscolaCalendario
		};
		
		function getIdEscolaCalendario(idTurma){
			return conexaoService.executar(escolaCalendarioQuery,[idTurma])
			.then(function(response){
				return conversorService.toObject(response.rows);
			});
		}
		
		
		
		return service;
	}
	
	var a = "select * from escola_calendario";
	
	var b = "select \r\n" + 
			"id_calendario \r\n" + 
			"from \r\n" + 
			"escola_calendario ec \r\n" + 
			"	inner join unidade_funcional uf on uf.id_unidade_funcional = ec.id_unidade_funcional";	
	
	var escolaCalendarioQuery =
		"select \r\n" + 
		"id_calendario \r\n" + 
		"from \r\n" + 
		"escola_calendario ec \r\n" + 
		"	inner join unidade_funcional uf on uf.id_unidade_funcional = ec.id_unidade_funcional \r\n" + 
		"	inner join turma t on t.id_unidade_funcional = uf.id_unidade_funcional \r\n" + 
		"	inner join ano_semestre asem on asem.id_ano_semestre  = t.id_ano_semestre and asem.ano = ec.ano\r\n" + 
		"where \r\n" + 
		" t.id_turma = ?\r\n" + 
		"";
	
	
})();