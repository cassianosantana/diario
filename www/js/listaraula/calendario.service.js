(function(){
	angular.module('diario.calendarioService',[])
		.factory('calendarioService',calendarioService);
	
	calendarioService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];
	
	function calendarioService(conexaoService, conversorService, formatadorService){
		
		var service = {
				isDataValidaCalendarioPeriodo:isDataValidaCalendarioPeriodo
		};
		return service
		
		function isDataValidaCalendarioPeriodo(data, idCalendario){
			return conexaoService.executar(calendarioPeriodoQuery,[idCalendario, formatadorService.formatatarDataSql(data)]) /* idCalendario, formatadorService.formatatarDataSql(data)*/
			.then(function(response){
				if(response.rows.length > 0){
					return conversorService.toArray(response.rows)
				}else{
					throw new Error("A turma não possui calendário para o periodo selecionado.");
				}
			})
			.then(function(response){
				var isDataValida = (response.length > 0)
				return isDataValida;
			});
			
		}
		
		function recuperarOrdemCalendarioPeriodo(data){
			return conexaoService.executar(ordemCalendarioQuery,[]);
		}
	}
	
	var calendarioPeriodoQuery = 
		"select null \n" +
		"where exists \n" +
		" ( \n" +
		"	select id_calendario \n" +
		" 	from \n" +
		"	calendario_periodo \n" +
		"	where id_calendario = ? \n" +
		"	and ? between date(data_inicio) and date(data_final) \n" +
		" )"
		
	var ordemCalendarioQuery = 
		"select ordem \r\n" + 
		" 	from \r\n" + 
		"	calendario_periodo \r\n" + 
		"	where id_calendario = ? \r\n" + 
		"	and ? between date(data_inicio) and date(data_final) \r\n" + 
		"";
	
})();