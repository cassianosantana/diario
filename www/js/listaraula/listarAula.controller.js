(function(){
	angular.module('diario.listarAulaController',[])
		.controller('listarAulaController',listarAulaController);
	listarAulaController.$inject = ['$ionicViewSwitcher',
                                    '$state',
                                    '$ionicSideMenuDelegate',
                                    'listarAulaService',
                                    '$ionicPopup',
                                    'listarUnidadeService',
                                    'listarTurmaService',
                                    'componenteCurricularService',
                                    'menuDireitoService',
                                    'conversorService',
                                    'cursoCalendarioService',
                                    'escolaCalendarioService',
                                    'turmaSerieService',
                                    'saltoService'];

	function listarAulaController( $ionicViewSwitcher, $state, $ionicSideMenuDelegate, listarAulaService, $ionicPopup, listarUnidadeService,
					listarTurmaService,componenteCurricularService,menuDireitoService, conversorService, cursoCalendarioService, escolaCalendarioService, turmaSerieService, saltoService ){

		var ctx = this;

		// binds do modelo de listar aula
		//manter ordem alfabetica

		ctx.abrirMenu = abrirMenu;
		ctx.backView = definirViewAnterior();
		ctx.componenteCurricularSelecionado = recuperarComponenteSelecionado($state.params.idComponente);
		ctx.dadosTurma = prepararDadosTurmaDatePicker($state.params.idUnidade, $state.params.idTurma, $state.params.idComponente);
		ctx.dataSelecionada = recuperarDataAtual();
		ctx.idTurma = $state.params.idTurma;
		ctx.idComponente = $state.params.idComponente;
		ctx.datePickerObject = recuperarDatePickerObject();
		ctx.isMenuAberto = isMenuAberto
		ctx.lancarFrequencia = lancarFrequencia;
		ctx.turmaSelecionada = recuperarTurmaSelecionada($state.params.idTurma);
		ctx.unidadeSelecionada = recuperarUnidadeSelecionada($state.params.idUnidade)
		ctx.voltar = voltar;



		// implementacao dos binds dos modelos
		// manter em ordem alfabetica

		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}

		function definirViewAnterior(){
			saltoService.definirBackViewData()
			return saltoService.getBackViewData()
		}

		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}

		function lancarFrequencia(dataSelecionada, idTurma, idComponente){
			escolaCalendarioService.getIdEscolaCalendario(idTurma)
			.then(function(response){
				return response.id_calendario;
			})
			.then(function(idCalendario){
				return listarAulaService.isDataValidaTurmaComponente(dataSelecionada, idCalendario, idTurma, idComponente)
					.then(function(response){
						return response
					})
			})
			.then(function(response){
				menuDireitoService.setDataAtual(dataSelecionada)
				$ionicViewSwitcher.nextDirection('forward');
				$state.go("lancarAlunoFrequencia",{
					"idUnidade":$state.params.idUnidade,
					"idTurma":$state.params.idTurma,
					"idComponente":$state.params.idComponente,
					"data":dataSelecionada
				});
			},function(error){
				$ionicPopup.alert({
					title: 'Data inválida',
				     template: error.message
				});
				return;
			})
		}

		function prepararDadosTurmaDatePicker(idUnidade, idTurma, idComponente){
			return{
				id_turma:idTurma,
				id_componente:idComponente,
				id_unidade:idUnidade
			};
		}

		function recuperarComponenteSelecionado(idComponente){
			if(!idComponente){
				return "Regência de classe"
			}
			return componenteCurricularService.getComponente(idComponente)
			.then(function(response){
			return ctx.componenteCurricularSelecionado = conversorService.toObject(response.rows).descricao;
			});
		}

		function recuperarDataAtual(){
			return new Date();
		}

		function recuperarDatePickerObject(){
			return datepickerObject = {
				      titleLabel: 'Selecione a data',  //Optional
				      todayLabel: 'Hoje',  //Optional
				      closeLabel: 'Voltar',  //Optional
				      setLabel: 'Ok',  //Optional
				      setButtonType : 'button-assertive',  //Optional
				      todayButtonType : 'button-assertive',  //Optional
				      closeButtonType : 'button-assertive',  //Optional
				      inputDate: ctx.dataSelecionada,  //Optional
				      callback: function (val) {  //Mandatory
				    	  ctx.dataSelecionada = val;
				      },
				      dateFormat: 'dd-MM-yyyy', //Optional
				      closeOnSelect: false, //Optional
				    };
		}

		function recuperarTurmaSelecionada(idTurma){
			return listarTurmaService.getTurma(idTurma)
			.then(function(response){
			 	ctx.turmaSelecionada = conversorService.toObject(response.rows).nome;
			});
		}

		function recuperarUnidadeSelecionada(idUnidade){
			return listarUnidadeService.getUnidade(idUnidade)
			.then(function(response){
				ctx.unidadeSelecionada = conversorService.toObject(response.rows).nome;
			});
		}

		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		}
	}
})();
