(function(){
	angular.module('diario.quadroHorarioService',[])
		.factory('quadroHorarioService', quadroHorarioService);
	
	quadroHorarioService.$inject = ['conexaoService', 'conversorService']
	
	function quadroHorarioService(conexaoService, conversorService){
		var service = {
				isDiaAulaTurmaComponente:isDiaAulaTurmaComponente,
				isDiaAulaTurmaRegencia:isDiaAulaTurmaRegencia
		};
		return service;
		
		function isDiaAulaTurmaComponente(idTurma, idComponente, diaSemana){
			return conexaoService.executar(diaAulaQuadroHomologadoComponente,[idTurma,idComponente,diaSemana])
			.then(function(response){
				if(response.rows.length > 0){
					return {
						isDataValidaCalendario:true,
						isDataEspecial:false
					};
				}else{
					throw new Error("A turma não possui aula para o componente selecionado nesta data. ")
				}
			});
		}
		
		function isDiaAulaTurmaRegencia(idTurma, diaSemana){
			return conexaoService.executar(diaAulaQuadroHomologadoRegencia,[idTurma, diaSemana])
			.then(function(response){
				if(response.rows.length >0){
					return{
						isDataValidaCalendario:true,
						isDataEspecial:false
					};
				}else{
					throw new Error("A turma não possui aula na data selecionada.");
				}
			})
		}
	}
		
	var diaAulaQuadroHomologadoComponente =
		"select 1 \r\n" + 
		"where \r\n" + 
		"exists (select 	qhh.id_quadro_horario_homologado\r\n" + 
		"	from turma_quadro_homologado tqh\r\n" + 
		"		inner join quadro_horario_homologado qhh on tqh.id_turma_quadro_homologado= qhh.id_turma_quadro_homologado\r\n" + 
		"		inner join horario_quadro_homologado hqh on tqh.id_turma_quadro_homologado = hqh.id_turma_quadro_homologado\r\n" + 
		"		inner join dia_semana ds on ds.dia_semana = qhh.dia_semana\r\n" + 
		"	where \r\n" + 
		"		tqh.id_turma = ?\r\n" + 
		"		and qhh.id_componente_curricular = ?\r\n" + 
		"		and ds.dia_semana_postgresql = ?\r\n" + 
		")";
		
	var diaAulaQuadroHomologadoRegencia = 
	"select 1 \r\n" + 
	"where \r\n" + 
	"exists (select 	qhh.id_quadro_horario_homologado\r\n" + 
	"	from turma_quadro_homologado tqh\r\n" + 
	"		inner join quadro_horario_homologado qhh on tqh.id_turma_quadro_homologado= qhh.id_turma_quadro_homologado\r\n" + 
	"		inner join horario_quadro_homologado hqh on tqh.id_turma_quadro_homologado = hqh.id_turma_quadro_homologado\r\n" + 
	"		inner join dia_semana ds on ds.dia_semana = qhh.dia_semana\r\n" + 
	"	where \r\n" + 
	"		tqh.id_turma = ?\r\n" + 
	"		and ds.dia_semana_postgresql = ?)";
	
})();