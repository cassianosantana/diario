(function(){
	angular.module('diario.listarAulaService',[])
		.factory('listarAulaService',listarAulaService);
	
	listarAulaService.$inject = ['$state', 'calendarioService', 'calendarioDataService', 'quadroHorarioService','$q', 'listarTurmaService'];
	
	function listarAulaService($state, calendarioService, calendarioDataService, quadroHorarioService, $q, listarTurmaService){
		var service = {
				isDataValidaTurmaComponente:isDataValidaTurmaComponente,
				getDataSelecionada:getDataSelecionada,
				validarDiaSemana:validarDiaSemana,
		};
		return service;
		
		function isDataValidaTurmaComponente(data,idCalendario, idTurma, idComponente){
			var isValid = false;
			var dataAtual = new Date();
			isValid = (dataAtual <= data?false:true);
			if(!isValid){
				throw new Error("A data Selecionada não pode ser maior que a data atual."); 
			}
			return $q.all({
				isDataValidaCalendario:calendarioService.isDataValidaCalendarioPeriodo(data, idCalendario),
				isDataEspecialCalendario:calendarioDataService.isDataEspecial(idCalendario,data),
				isTurmaRegencia:listarTurmaService.isTurmaRegenciaClasse(idTurma)
			})
			.then(function(response){
				if(response.isTurmaRegencia === "true"){
					return quadroHorarioService.isDiaAulaTurmaRegencia(idTurma, data.getDay());
				}else{
					return quadroHorarioService.isDiaAulaTurmaComponente(idTurma, idComponente, data.getDay())
				}
			});
		}
		
		function getDataSelecionada(){
			return(!state.params.data ? null:new Date($state.params.data));	
		}
		
		function recuperarHorarioAula(idTurma,idComponente,dataSelecionada){
			
		}
		
		function validarDiaSemana(data){
			return data.getDay();
		}
	};
	
})();