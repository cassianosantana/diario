(function(){
	angular.module('diario.cursoCalendarioService',[])
		.factory('cursoCalendarioService',cursoCalendarioService);

	cursoCalendarioService.$inject = ['conexaoService', 'conversorService']

	function cursoCalendarioService(conexaoService, conversorService){
		var service = {
				getIdCursoCalendario:getIdCursoCalendario
		}
		return service;

		function getIdCursoCalendario(idUnidadeFuncional, idCurso, ano){
			return conexaoService.executar(cursoCalendarioQuery,[idUnidadeFuncional, idCurso, ano])
			.then(function(response){
				if(response.rows.length == 0){
					return {
						id_calendario_curso:null
					}
				}
				return conversorService.toObject(response.rows);
			})
		}
	}

	var cursoCalendarioQuery =
		"select \r\n" +
		"curso_calendario.id_calendario as id_calendario_curso\r\n" +
		"from curso_calendario \r\n" +
		"	inner join unidade_funcional uf on uf.id_unidade_funcional = curso_calendario.id_unidade_funcional\r\n" +
		"	inner join turma t on t.id_unidade_funcional = uf.id_unidade_funcional \r\n" +
		"	inner join turma_serie ts on ts.id_turma = t.id_turma \r\n" +
		"	inner join serie s on s.id_serie = ts.id_serie\r\n" +
		"	inner join curso c on c.id_curso = s.id_curso\r\n" +
		"	inner join ano_semestre asem on asem.id_ano_semestre = t.id_ano_semestre\r\n" +
		"\r\n" +
		"where\r\n" +
		"uf.id_unidade_funcional = ? \r\n" +
		"and c.id_curso = ?\r\n" +
		"and asem.ano = ?\r\n" +
		"	 ";
})();
