(function() {
  angular.module('diario.popularTabelasService', []).factory('popularTabelasService',
      popularTabelasService);

  popularTabelasService.$inject = ['conexaoService', '$q', '$http']

  function popularTabelasService(conexaoService, $q, $http) {
    // manter ordem alfabetica
    var service = {};

    service.TABELAS = ['unidadeFuncional',
      'gradeCurricular',
      'turma',
      'disciplina',
      'componenteCurricular',
      'cidadao',
      'alunoMatrStatus*',
      'alunoMatricula',
      'alunoMatrTurma',
      'diaSemana*',
      'horarioQuadroHomologado',
      'quadroHorarioHomologado',
      'turmaQuadroHomologado',
      'calendario',
      'calendarioData',
      'calendarioPeriodo',
      'calendarioRecesso',
      'cursoCalendario',
      'escolaCalendario',
      'motivoDiaLetivoExtra*',
      'motivoDiaNaoLetivo*',
      'motivoEvento*',
      'anoSemestre',
      'aluno',
      'motivoFaltaJustificada*',
      'curso',
      'serie',
      'turno',
      'faltaAlunoDisciplinaDiario',
      'turmaComponente',
      'turmaComponenteServidor',
      'horarioTurma*',
      'turmaSerie',
      'turmaRegenciaAtribuicao',
      'turmaRegenciaAtribSrv',
      'faltaAlunoGlobalizadaDiario',
      'notaPeriodoAluno*',
      'registroFaltaDisciplinaDiario',
      'registroFaltaGlobalDiario'];

		service.getPath = function(nomeArquivo, idUsuario){
			idUsuario = idUsuario || "";
			if(!nomeArquivo){
        throw new Error("O nome do arquivo de destino não é valido");
			}
			return "js/carga/json/" + nomeArquivo + (idUsuario ? "_" + idUsuario : "") + ".json";
    };

    service.gerarChaves = function(item) {
      return Object.keys(item);
    };

    service.nomeSql = function(nome) {
      return nome.replace(/([A-Z])/g, function(letra) { return "_" + letra.toLowerCase(); });
    };

    service.inserirRegistro = function(tabela, registro) {
      var campos = [];
      var args = [];
      var marcas = [];
      angular.forEach(registro, function(value, key) {
        campos.push(key);
        args.push(value);
        marcas.push("?");
      });
      var sql = "insert or ignore into " + tabela +
        " (" + campos.join(",") + ") values (" +
        marcas.join(",") + ")";
      return conexaoService.executar(sql, args);
    };

    service.inserirRegistros = function(tabela, registros) {
      if(registros.length == 0) {
        return $q.when(null);
      }
      var campos = [];
      var marcas = [];
      angular.forEach(registros[0], function(value, key) {
        campos.push(key);
        marcas.push("?");
      });
      var sql = "insert or ignore into " + tabela +
        " (" + campos.join(",") + ") values (" +
        marcas.join(",") + ")";
      var promises = [];
      angular.forEach(registros, function(registro) {
        var args = [];
        angular.forEach(campos, function(value) {
          args.push(registro[value]);
        });
        promises.push(conexaoService.executar(sql, args));
      });
      return $q.all(promises);
    };

    service.recuperarDadosPrimeiroLogin = function(idUsuario) {
      var promises = {};
      angular.forEach(service.TABELAS, function(tabela) {
        if(tabela.endsWith("*")) {
          var nomeTabela = tabela.replace("*", "");
          promises[nomeTabela] = $http.get(service.getPath(nomeTabela));
        } else {
          if(DEMONSTRACAO) {
            promises[tabela] = $http.get(service.getPath(tabela, idUsuario));
          }
        }
      });
      return $q.all(promises, function(response) {
        console.log("Dados recuperados.");
      },function(error){
        console.debug(error);
        return error;
      });
    };

    // popular as tabelas recem criadas
    service.popularTabelas = function(idUsuario) {
      var deferred = $q.defer();
      service.recuperarDadosPrimeiroLogin(idUsuario).then(function(response) {
        var promessas = [];
        angular.forEach(service.TABELAS, function(tabela) {
          var nomeTabela = tabela.replace("*", "");
          if(response[nomeTabela] == null) {
            console.log("FALTA DADOS: " + nomeTabela);
          } else {
            promessas.push(service.inserirRegistros(service.nomeSql(nomeTabela), response[nomeTabela].data));
          }
        });
        $q.all(promessas).then(function() {
          deferred.resolve();
        });
      });
      return deferred.promise;
    };

    return service;
  }
})();
