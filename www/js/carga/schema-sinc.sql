create table pacote_sincronizacao (
  tipo text not null, -- Tipo do pacote: frequencia_diaria_globalizada, frequencia_diaria_componente.
  id text not null, -- JSON da chave identificadora do pacote.
  operacao texto not null, -- Tipo de operacação: inclur, alterar, etc. Depende do tipo de pacote.
  conteudo text null, -- JSON do conteudo do pacote. Pode estar vazio e ser calculado apenas no momento da sincronizacao.
  data_criacao numeric not null, -- Data da criacao do pacote. Referente ao momento da alteracao no banco local.
  data_sincronizacao numeric null, -- Data da sincronizacao do pacote, caso tenha ocorrido.

  primary key (tipo, id)
);

