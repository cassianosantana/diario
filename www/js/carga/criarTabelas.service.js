(function(){
    angular.module('diario.criarTabelasService',[]).factory('criarTabelasService',criarTabelasService);

    criarTabelasService.$inject = ['$http', '$q', '$cordovaSQLite', 'conexaoService', 'menuDireitoService', 'conversorService'];

    function criarTabelasService($http, $q, $cordovaSQLite, conexaoService, menuDireitoService, conversorService){ 		

      var service = {};

      service.existeTabela = function(nomeTabela) {
        return conexaoService.executar( 
          "SELECT name FROM sqlite_master WHERE type='table' AND name=? ",[nomeTabela]).then(function(response){
          return (response.rows.length > 0);
          });
      };

      service.criarTabelas = function() {
        return service.existeTabela("cidadao").then(function(existe) {
          if(!existe) {
            return service.executarScript("js/carga/schema.sql").then(function() {
              return service.executarScript("js/carga/schema-sinc.sql");
            });
          } else {
            return false;
          }
        });
      };

      service.executarScript = function(caminho) {
        return $http.get(caminho).then(function(resultado) {
          var v = resultado.data.split(/;$/gm);
          var lista = [];
          angular.forEach(v, function(sql) {
            if(sql.trim() != '') {
              lista.push(conexaoService.executar(sql));
            }
          });
          return $q.all(lista).then(function() { return true; });
        });
      };

      return service;
    }
})();

