create table if not exists aluno(
  id_cidadao integer primary key,
  codigo integer,
  foreign key(id_cidadao) references cidadao(id_cidadao)
);

create table if not exists ano_semestre(
  id_ano_semestre integer primary key,
  ano integer,
  semestre integer
);

create table if not exists cidadao(
  id_cidadao integer primary key,
  nome text,
  nascimento_data numeric,
  sexo text,
  flg_necessidade_especial numeric
);

create table if not exists serie_duracao (
  id_serie_duracao integer primary key,
  descricao text
);

create table if not exists curso (
  id_curso integer primary key,
  sigla text,
  descricao text,
  serie_duracao integer,
  termo_reprovado_frequencia text,
  termo_reprovado text,
  termo_aprovado text,

  foreign key(serie_duracao) references serie_duracao(id_serie_duracao)
);

create table if not exists serie(
  id_serie integer primary key,
  sigla text,
  descricao text,
  id_curso integer,
  flg_apon_falta_globalizado numeric,
  flg_regencia_classe numeric,

  idade_distorcao integer,
  foreign key (id_curso) references curso(id_curso)
);

create table if not exists turno (
  id_turno integer,
  sigla text,
  descricao text
);

create table if not exists grade_curricular(
  id_grade_curricular integer primary key,
  descricao text,
  flg_ativo numeric,
  id_turno integer,
  id_serie integer,
  qtd_aulas_semanal integer,
  duracao_aula integer,

  foreign key (id_turno) references turno(id_turno),
  foreign key (id_serie) references serie(id_serie)
);

create table if not exists disciplina (
  id_disciplina integer primary key,
  sigla text,
  descricao text
);

create table if not exists motivo_falta_justificada(
  id_motivo_falta_justificada integer primary key,
  sigla text,
  descricao text,
  flg_ativo_numeric
);

create table if not exists motivo_dispensa_disciplina(
  id_motivo_dispensa_disciplina integer primary key,
  sigla text,
  descricao text
);

create table if not exists necessidade_especial(
  integer id_necessidade_especial primary key,
  sigla text,
  descricao text
);

create table if not exists nota_periodo_aluno(
  id_nota_periodo_aluno integer primary key,
  sigla text,
  descricao text
);

create table if not exists turma_status(
  id_turma_status integer primary key,
  descricao text
);

create table if not exists turma(
  id_turma integer primary key,
  nome text,
  codigo text,
  flg_organizada numeric,
  flg_multietapa numeric,
  flg_multiseriada numeric,
  id_ano_semestre integer,
  id_curso integer,
  id_turno integer,
  id_unidade_funcional integer,
  id_turma_status integer,
  id_grade_curricular integer,
  flg_frequencia_diaria numeric,
  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre),
  foreign key (id_curso) references curso (id_curso),
  foreign key (id_turno) references turno (id_turno),
  foreign key (id_turma_status) references turma_status(id_turma_status),
  foreign key (id_unidade_funcional) references unidade_funcional(id_unidade_funcional)
);

create table if not exists unidade_funcional(
  id_unidade_funcional integer primary key,
  codigo integer,
  nome text
);

create table if not exists aluno_matr_status(
  id_aluno_matr_status integer primary key,
  descricao text,
  flg_status_ativo numeric
);

create table if not exists aluno_matricula (
  id_aluno_matricula integer,
  id_unidade_funcional integer,
  id_ano_semestre integer,
  id_serie integer,
  data_inclusao numeric,
  data_matricula numeric,
  id_curso integer,
  id_cidadao integer,
  id_aluno_matr_status Integer,

  foreign key (id_unidade_funcional) references unidade_funcional(id_unidade_funcional),
  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre),
  foreign key (id_serie) references serie(id_serie),
  foreign key (id_curso) references curso(id_curso),
  foreign key (id_cidadao) references cidadao(id_cidadao),
  foreign key (id_aluno_matr_status) references aluno_matr_status(id_aluno_matr_status)
);

create table if not exists aluno_matr_turma (
  chamada integer,
  id_turma integer,
  data_enturmacao numeric,
  id_aluno_matricula integer,
  id_serie_enturmacao integer,

  primary key (id_turma,id_aluno_matricula),
  foreign key(id_turma) references turma(id_turma),
  foreign key(id_aluno_matricula) references aluno_matricula(id_aluno_matricula),
  foreign key(id_serie_enturmacao) references serie(id_serie)
);

create table if not exists turma_serie(
  id_turma integer,
  id_serie integer,
  id_grade_curricular integer,
  primary key(id_turma,id_serie),
  foreign key(id_turma) references turma(id_turma),
  foreign key(id_serie) references serie(id_serie),
  foreign key(id_grade_curricular) references grade_curricular(id_grade_curricular)
);

create table if not exists componente_curricular(
  id_componente_curricular integer primary key,
  qtd_aulas_semanal integer,
  flg_aponta_notas numeric,
  id_disciplina integer,
  id_grade_curricular integer,
  id_tipo_atribuicao_componente integer,
  flg_aponta_faltas numeric,
  flg_aponta_conteudo_curricular numeric,

  foreign key (id_disciplina) references disciplina(id_disciplina),
  foreign key (id_grade_curricular) references grade_curricular(id_grade_curricular)
);

create table if not exists horario_turma(
  id_horario_turma integer primary key,
  horario_inicio numeric,
  horario_termino numeric
);

create table if not exists falta_aluno_disciplina(
  qtd_faltas integer,
  id_disciplina integer,
  id_aluno_matricula integer,
  id_turma integer,
  id_nota_periodo_aluno integer,
  falta_justificada integer,
  falta_nao_justificada integer,

  primary key(id_turma, id_aluno_matricula,id_disciplina, id_nota_periodo_aluno),
  foreign key(id_disciplina) references disciplina(id_disciplina),
  foreign key(id_aluno_matricula) references aluno_matricula(id_aluno_matricula),
  foreign key(id_turma) references turma(id_turma),
  foreign key(id_nota_periodo_aluno) references nota_periodo_aluno(id_nota_perido_aluno)
);

create table if not exists falta_aluno_disciplina_diario(
  id_falta_aluno_disciplina_diario integer primary key,
  id_aluno_matricula integer,
  id_turma integer,
  id_disciplina integer,
  id_nota_periodo_aluno integer,
  data numeric,
  id_horario_turma integer,
  flg_justificada numeric,
  id_motivo_falta_justificada integer,

  foreign key (id_aluno_matricula) references aluno_matricula(id_aluno_matricula),
  foreign key (id_turma) references turma(id_turma),
  foreign key (id_disciplina) references disciplina(id_disciplina),
  foreign key (id_nota_periodo_aluno) references nota_periodo_aluno(id_nota_periodo_aluno),
  foreign key (id_horario_turma) references horario_turma (id_horario_turma),
  foreign key (id_motivo_falta_justificada) references motivo_falta_justificada(id_motivo_falta_justificada)
);

create table if not exists falta_aluno_globalizada(
  qtd_faltas integer,
  id_aluno_matricula integer,
  id_turma integer,
  id_nota_periodo_aluno integer,
  falta_justificada integer,
  falta_não_justificada integer,

  primary key (id_turma, id_aluno_matricula, id_nota_periodo_aluno),
  foreign key (id_aluno_matricula) references aluno_matricula(id_aluno_matricula),
  foreign key (id_turma)references turma(id_turma),
  foreign key (id_nota_periodo_aluno) references nota_periodo_aluno(id_nota_periodo_aluno)
);

create table if not exists falta_aluno_globalizada_diario(
  id_aluno_matricula integer,
  id_turma integer,
  id_nota_periodo_aluno integer,
  flg_justificada numeric,
  data numeric,
  id_motivo_falta_justificada integer,

  primary key (id_turma, id_aluno_matricula, id_nota_periodo_aluno, data),
  foreign key(id_aluno_matricula) references aluno_matricula(id_aluno_matricula),
  foreign key(id_turma) references turma(id_turma),
  foreign key (id_nota_periodo_aluno) references nota_periodo_aluno(id_nota_periodo_aluno),
  foreign key(id_motivo_falta_justificada) references motivo_falta_justificada(id_motivo_falta_justificada)
);

create table if not exists servidor_cargo_base(
  id_servidor_cargo_base integer primary key,
  flg_aberto numeric,
  id_cidadao integer,
  id_cargo integer,

  foreign key(id_cidadao) references cidadao(id_cidadao),
  foreign key(id_cargo) references cargo(id_cargo)
);

create table if not exists turma_regencia_atrib_status (
  id_turma_regemcia_atrib_status integer primary key,
  sigla text,
  descricao text,
  flg_ativo numeric
);

create table if not exists turma_regencia_atribuicao(
  id_turma_regencia_atribuicao integer primary key,
  aulas_semanal_qtd integer,
  id_turno integer,
  id_turma integer,
  id_disciplina integer,
  id_unidade_funcional integer,
  id_curso inetger,
  id_ano_semestre integer,
  id_turma_regencia_atrib_status integer,
  flg_turma_especial numeric,

  foreign key(id_turma) references turma(id_turma),
  foreign key(id_turno) references turno(id_turno),
  foreign key (id_disciplina) references disciplina(id_disciplina),
  foreign key (id_unidade_funcional) references unidade_funcional(id_unidade_funcional),
  foreign key (id_curso) references curso(id_curso),
  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre),
  foreign key (id_turma_regencia_atrib_status) references turma_regencia_atrib_status(id_turma_regencia_atrib_status)
);

create table if not exists turma_regencia_atrib_srv(
  id_turma_regencia_atrib_srv integer primary key,
  data_atribuicao numeric,
  id_servidor_cargo_base integer,
  id_turma_regencia_atribuicao integer,
  id_cidadao integer,
  id_ano_semestre integer,
  flg_jornada_opcao numeric,

  foreign key (id_servidor_cargo_base) references servidor_cargo_base(id_servidor_cargo_base),
  foreign key(id_turma_regencia_atribuicao) references turma_regencia_atribuicao(id_turma_regencia_atribuicao),
  foreign key(id_cidadao) references cidadao(id_cidadao),
  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre)
);

create table if not exists turma_componente_status(
  id_turma_componente_status integer primary key,
  sigla text,
  descricao text,
  flg_ativo numeric
);

create table if not exists turma_componente(
  id_turma_componente integer primary key,
  id_turma integer,
  id_turno integer,
  id_componente_curricular integer,
  id_unidade_funcional integer,
  id_curso integer,
  id_turma_componente_status integer,
  id_ano_semestre integer,

  foreign key(id_turma) references turma(id_turma),
  foreign key(id_turno) references tuno(id_turno),
  foreign key(id_componente_curricular) references componente_curricular(id_componente_curricular),
  foreign key(id_unidade_funcional) references unidade_funcional(id_unidade_funcional),
  foreign key(id_curso) references curso(id_curso),
  foreign key(id_turma_componente_status) references turma_componente_status(id_turma_componente_status),
  foreign key(id_ano_semestre) references ano_semestre(id_ano_semestre)
);

create table if not exists turma_componente_servidor(
  id_turma_componente_servidor integer primary key,
  id_servidor_cargo_base integer,
  id_turma_componente integer,
  id_cidadao integer,
  id_ano_semestre integer,
  flg_jornada_opcao numeric,

  foreign key (id_servidor_cargo_base) references servidor_cargo_base(id_servidor_cargo_base),
  foreign key (id_turma_componente) references turma_componente(id_turma_componente),
  foreign key (id_cidadao) references cidadao(id_cidadao),
  foreign key (id_ano_semestre) references ano_semestre(id_ano_semestre)
);

create table if not exists dia_semana(
  dia_semana integer primary key,
  descricao text,
  dia_semana_postgresql integer,
  dia_semana_java integer
);

create table if not exists turno_horario (
  id_turno_horario integer primary key,
  sigla text
  descricao text
);

create table if not exists turma_quadro_homologado(
  id_turma_quadro_homologado integer primary key,
  id_turma integer,
  data_inicio_vigencia numeric,
  data_termino_vigencia numeric,
  data_homologacao numeric,

  foreign key (id_turma) references turma(id_turma)
);

create table if not exists horario_quadro_homologado(
  id_horario_quadro_homologado integer primary key,
  id_turma_quadro_homologado integer,
  horario_inicio numeric,
  horario_termino numeric,

  foreign key(id_turma_quadro_homologado) references turma_quadro_homologado(id_turma_quadro_homologado)
);

create table if not exists quadro_horario_homologado(
  id_quadro_horario_homologado integer primary key,
  id_turma_quadro_homologado integer,
  id_horario_quadro_homologado integer,
  dia_semana integer,
  id_componente_curricular integer,
  id_cidadao integer,
  id_turno_horario  integer,

  foreign key (id_horario_quadro_homologado) references horario_quadro_homologado(id_horario_quadro_homologado),
  foreign key (id_turma_quadro_homologado) references turma_quadro_homologado(id_turma_quadro_homologado),
  foreign key(dia_semana) references dia_semana(dia_semana),
  foreign key (id_componente_curricular) references componente_curricular(id_componente_curricular),
  foreign key (id_cidadao) references cidadao(id_cidadao)
);

create table if not exists motivo_dia_letivo_extra(
  id_motivo_dia_letivo_extra integer primary key,
  descricao text,
  flg_ativo numeric,
  legenda text
);

create table if not exists motivo_dia_nao_letivo(
  id_motivo_dia_nao_letivo integer primary key,
  descricao text,
  flg_ativo numeric,
  legenda text
);

create table if not exists motivo_evento(
  id_motivo_evento integer primary key,
  descricao text,
  flg_ativo numeric,
  legenda text
);

create table if not exists calendario(
  id_calendario integer primary key,
  ano integer,
  qtd_dias_letivos integer,
  descricao text,
  id_unidade_funcional integer,
  flg_curso numeric,
  flg_aprovado numeric,
  inicio_encerramento_letivo numeric,
  termino_encerramento_letivo numeric,

  foreign key (id_unidade_funcional) references unidade_funcional(id_unidade_funcional)
);

create table if not exists curso_calendario(
  id_unidade_funcional integer,
  id_curso integer,
  ano integer,
  id_calendario integer,

  primary key(id_unidade_funcional, id_curso, ano),
  foreign key(id_unidade_funcional) references unidade_funcional(id_unidade_funcional),
  foreign key(id_curso) references curso (id_curso),
  foreign key(id_calendario) references calendario (id_calendario)
);

create table if not exists calendario_data(
  id_motivo_dia_letivo_extra integer,
  id_motivo_dia_nao_letivo integer,
  id_calendario integer,
  id_motivo_evento integer,
  data numeric,

  primary key(id_calendario, data),
  foreign key(id_motivo_dia_letivo_extra) references motivo_dia_letivo_extra(id_motivo_dia_letivo_extra),
  foreign key(id_motivo_dia_nao_letivo) references motivo_dia_nao_letivo(id_motivo_dia_nao_letivo),
  foreign key(id_calendario) references calendario(id_calendario),
  foreign key(id_motivo_evento) references motivo_evento(id_motivo_evento)
);

create table if not exists calendario_periodo(
  id_calendario_periodo integer primary key,
  id_calendario integer,
  ordem integer,
  data_inicio numeric,
  data_final numeric,
  qtd_semanas_letivas integer,
  qtd_dias_letivos integer,

  foreign key(id_calendario) references calendario(id_calendario)
);

create table if not exists calendario_recesso(
  id_calendario integer,
  data_inicio numeric,
  data_fim numeric,

  primary key(id_calendario, data_inicio),
  foreign key(id_calendario) references calendario(id_calendario)
);

create table if not exists escola_calendario(
  id_unidade_funcional integer,
  ano integer,
  id_calendario integer,

  foreign key(id_unidade_funcional) references unidade_funcional(id_unidade_funcional),
  foreign key(id_calendario) references calendario(id_calendario)
);

create table if not exists motivo_falta_justificada(
  id_motivo_falta_justificada integer primary key,
  sigla text,
  descricao text,
  flg_ativo_numeric
);

create table if not exists registro_falta_disciplina_diario(
  id_turma integer,
  id_disciplina integer,
  data_inclusao numeric,
  data_atualizacao numeric,
  data_referencia numeric,
  flg_concluido numeric,

  primary key (id_turma, id_disciplina, data_referencia)
);

create table if not exists  registro_falta_global_diario(
  id_turma integer,
  data_inclusao numeric,
  data_atualizacao numeric,
  data_referencia numeric,
  flg_concluido numeric,
  primary key(id_turma, data_referencia)
);
