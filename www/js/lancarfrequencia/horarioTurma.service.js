(function(){
	angular.module('diario.horarioTurmaService',[]).factory('horarioTurmaService',horarioTurmaService);
	
    horarioTurmaService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];
	
	function horarioTurmaService(conexaoService, conversorService, formatadorService) {
    var service = {};
		
		service.recuperarIdHorarioTurma = function(horarioInicio, horarioTermino) {
			return conexaoService.executar(recuperarHorarioTurmaQuery, 
        [formatadorService.formatatarHoraSql(horarioInicio), 
          formatadorService.formatatarHoraSql(horarioTermino)]).then(function(response){
				return conversorService.toObject(response.rows).id_horario_turma;
			});
    };

		return service;
	}
		
	
	var recuperarHorarioTurmaQuery = 
		"SELECT \n" + 
		"ht.id_horario_turma\n" + 
		"FROM \n" + 
		"horario_turma ht\n" + 
		"WHERE \n" + 
		"	ht.horario_inicio = ?" + 
    "	AND ht.horario_termino = ?";
})();
