(function (){
	angular.module('diario.lancarFrequenciaController',[])
		.controller('lancarFrequenciaController',lancarFrequenciaController);
	
	
	lancarFrequenciaController.$inject = [ '$state',
	                                       '$ionicViewSwitcher',
	                                       '$ionicSideMenuDelegate',
	                                       'lancarAlunoFrequenciaService',
	                                       'conversorService',
	                                       'horarioQuadroService',
	                                       'horarioFaltaService',
	                                       'atualizarFaltaAlunoDisciplinaDiarioService',
	                                       'listarTurmaService',
	                                       'atualizarFaltaAlunoGlobalizadaDiarioService',
	                                       'listarTurmaService',
	                                       '$ionicPlatform'
	                                      ];
	
	function lancarFrequenciaController($state, $ionicViewSwitcher, $ionicSideMenuDelegate,
			lancarAlunoFrequenciaService, conversorService,horarioQuadroService, horarioFaltaService, atualizarFaltaAlunoDisciplinaDiarioService,listarTurmaService, atualizarFaltaAlunoGlobalizadaDiarioService, listarTurmaService, $ionicPlatform){
		var ctx = this;
		
		// binds do modelo de lancar frequencia
		// manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.aluno = recuperarAlunoSelecionado($state.params.idAluno);
		atualizarDadosLancamentoFalta($state.params.idAluno, $state.params.idComponente, $state.params.idTurma, new Date($state.params.data))
		ctx.componenteSelecionado = $state.params.idComponente;
		ctx.dataSelecionada = new Date($state.params.data);
		ctx.alunoSelecionado = $state.params.idAluno;
		ctx.detalhar = detalhar;
		ctx.horarios = listarHorariosFaltaDiario($state.params.idTurma, $state.params.idComponente, new Date($state.params.data).getDay());
		ctx.lancarFaltaAluno = lancarFaltaAluno
		ctx.isMenuAberto = isMenuAberto;
		ctx.turmaSelecionada = $state.params.idTurma;
		ctx.voltar = voltar
		
		// colocando o evento de gravar falta no back button do hardware em caso de android
		
		
		//implementacoes dos binds dos modelos 
		// manter ordem alfabetica
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}
		
		function atualizarDadosLancamentoFalta(idAluno, idComponente, idTurma, data){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return atualizarDadosLancamentoFaltaTurmaRegencia(idAluno, idTurma, data);
				}else{
					return atualizarDadosLancamentoFaltaTurmaComponente(idAluno, idComponente, idTurma, data);
				}
			});
		}
		
		function atualizarDadosLancamentoFaltaTurmaComponente(idAluno, idComponente, idTurma, data){
			atualizarFaltaAlunoDisciplinaDiarioService.setData(data);
			atualizarFaltaAlunoDisciplinaDiarioService.setIdAluno(idAluno);
			atualizarFaltaAlunoDisciplinaDiarioService.setIdTurma(idTurma);
			atualizarFaltaAlunoDisciplinaDiarioService.setIdComponente(idComponente);
			return;
		}
		
		function atualizarDadosLancamentoFaltaTurmaRegencia(idAluno, idTurma, data){
			atualizarFaltaAlunoGlobalizadaDiarioService.setData(data);
			atualizarFaltaAlunoGlobalizadaDiarioService.setIdAluno(idAluno),
			atualizarFaltaAlunoGlobalizadaDiarioService.setIdTurma(idTurma);
			return 
		}
		
		function detalhar(){
			$ionicViewSwitcher.nextDirection('forward');
			$state.go("informacoesAluno",$state.params);
		}
		
		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}
		
		function lancarFaltaAluno(idTurma){	
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return atualizarFaltaAlunoGlobalizadaDiarioService.gravarFalta()
				}else{
					return atualizarFaltaAlunoDisciplinaDiarioService.gravarFalta();
				}
			})
			.then(function(response){
				$ionicViewSwitcher.nextDirection('back');
				$state.go("lancarAlunoFrequencia",$state.params);
			});
			
		}
		
		function listarHorariosFaltaDiario(idTurma, idComponente, diaSemana){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return listarHorariosFaltaGlobalizadaDiario(idTurma, diaSemana);
				}else{
					return listarHorariosFaltaDisciplinaDiario(idTurma, idComponente, diaSemana);
				}
			});
		}
		
		function listarHorariosFaltaDisciplinaDiario(idTurma, idComponente, diaSemana){
			horarioQuadroService.recuperarHorariosDiaTurmaComponente(idTurma, idComponente, diaSemana)
			.then(function(response){
				atualizarFaltaAlunoDisciplinaDiarioService.setHorariosFalta(conversorService.toArray(response.rows));
				return ctx.horarios = conversorService.toArray(response.rows)
			});
		}
		
		function listarHorariosFaltaGlobalizadaDiario(idTurma, diaSemana){
			horarioQuadroService.recuperarHorarioDiaTurmaRegencia(idTurma, diaSemana)
			.then(function(response){
				return ctx.horarios = conversorService.toArray(response.rows);
			})
		}
		
		function recuperarAlunoSelecionado(idAlunoMatricula){
			return lancarAlunoFrequenciaService.getAluno(idAlunoMatricula)
			.then(function(response){
			 ctx.aluno = conversorService.toObject(response.rows);
			});
		}
		
		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		}		
	}
})();