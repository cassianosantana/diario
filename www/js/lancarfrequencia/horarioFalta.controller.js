(function(){
	angular.module('diario.horarioFaltaController',[])
		.controller('horarioFaltaController',horarioFaltaController);
	
	horarioFaltaController.$inject = ['horarioFaltaService', 
	                                  'conversorService', 
	                                  'listarTurmaService', 
	                                  'componenteCurricularService', 
	                                  'faltaAlunoDisciplinaDiarioService',
	                                  'formatadorService', 
	                                  'atualizarFaltaAlunoDisciplinaDiarioService',
	                                  'faltaAlunoGlobalizadaDiarioService',
	                                  'atualizarFaltaAlunoGlobalizadaDiarioService'];
	
	function horarioFaltaController(horarioFaltaService, conversorService, listarTurmaService, componenteCurricularService, faltaAlunoDisciplinaDiarioService, formatadorService, 
			atualizarFaltaAlunoDisciplinaDiarioService, faltaAlunoGlobalizadaDiarioService, atualizarFaltaAlunoGlobalizadaDiarioService){
		var ctx = this;
		
		// binds do modelo de horario falta controller
		// manter ordem alfabetica
		ctx.ID_FALTA_JUSTIFICADA = horarioFaltaService.ID_FALTA_JUSTIFICADA;
		ctx.ID_FALTA_NAO_JUSTIFICADA = horarioFaltaService.ID_FALTA_NAO_JUSTIFICADA;
		ctx.marcarFalta = marcarFalta
		recuperarFalta(ctx.turmaSelecionada, ctx.componenteSelecionado, ctx.data, ctx.horarioInicio, ctx.horarioFim,ctx.alunoSelecionado);
		ctx.motivos = recuperarMotivoFaltaJustificada();
		ctx.setMotivo = setMotivo
		
		// implementacoes dos binds do modelo horario falta controller
		// manter ordem alfabetica
		
		function atribuirFalta(registroFalta){
			if(registroFalta.flg_justificada === "true"){
				ctx.faltaJustificada = true;
				marcarFalta(ctx.ID_FALTA_JUSTIFICADA, ctx.faltaJustificada, ctx.turmaSelecionada);
				
				setMotivo(registroFalta.id_motivo_falta_justificada, ctx.turmaSelecionada);
				ctx.motivoSelecionado = registroFalta.id_motivo_falta_justificada;
			}else{
				ctx.faltaNaoJustificada = true;
				marcarFalta(ctx.ID_FALTA_NAO_JUSTIFICADA,ctx.faltaNaoJustificada, ctx.turmaSelecionada);
				
			}
		}
		
		function marcarFalta(falta,isMarcada, idTurma){
			if(isMarcada){
				ctx.faltaJustificada = (falta === horarioFaltaService.ID_FALTA_JUSTIFICADA);
				ctx.faltaNaoJustificada = (falta === horarioFaltaService.ID_FALTA_NAO_JUSTIFICADA);
				
			}
			
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return atualizarFaltaAlunoGlobalizadaDiarioService.marcarFalta(ctx.faltaJustificada, ctx.faltaNaoJustificada,ctx.motivoSelecionado);
				}else{
					return atualizarFaltaAlunoDisciplinaDiarioService.marcarFalta(ctx.horarioInicio, ctx.horarioFim, 
							ctx.faltaJustificada, ctx.faltaNaoJustificada,ctx.motivoSelecionado);
				}
			});
		}
		
		function recuperarMotivoFaltaJustificada(){
			return horarioFaltaService.getMotivosFaltaJustificada()
				.then(function(response){
					return ctx.motivos = conversorService.toArray(response.rows);
			});
		}
		
		function recuperarFalta(idTurma, idComponente, data, horarioInicio, horarioFim, idAluno){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return faltaAlunoGlobalizadaDiarioService.faltaAlunoData(idAluno, idTurma, data);
				}else{
					return faltaAlunoDisciplinaDiarioService.faltaAlunoHorario(idTurma, idComponente,data, horarioInicio, horarioFim, idAluno);
				}
				
			})
			.then(function(response){
				if(response.length && response.length >0){
					atribuirFalta(response[0]);
				}
				return;
			});
		}
		
		function setMotivo(motivo, idTurma){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(response){
				if(response === "true"){
					return atualizarFaltaAlunoGlobalizadaDiarioService.setMotivo(motivo,idTurma);
				}else{
					return atualizarFaltaAlunoDisciplinaDiarioService.setMotivo(motivo, idTurma);
				}
			})
			
		}
	}
})();