(function(){
	angular.module('diario.atualizarFaltaAlunoDisciplinaDiarioService',[]).factory(
	  'atualizarFaltaAlunoDisciplinaDiarioService',atualizarFaltaAlunoDisciplinaDiarioService);
	
  atualizarFaltaAlunoDisciplinaDiarioService.$inject = [
    'conversorService', 
    'conexaoService', 
    'formatadorService', 
    'horarioTurmaService', 
    'sincronizacaoService',
    '$q'];
	
	function atualizarFaltaAlunoDisciplinaDiarioService(
	    conversorService, 
	    conexaoService, 
	    formatadorService, 
	    horarioTurmaService, 
	    sincronizacaoService,
	    $q){

		// manter ordem alfabetica
		var service = {
				excluirRegistroFaltaAluno:excluirRegistroFaltaAluno,
				inserirRegistroFaltaAluno:inserirRegistroFaltaAluno,
				gravarRegistroFaltaAluno:gravarRegistroFaltaAluno,
				gravarFalta:gravarFalta,
				marcarFalta:marcarFalta,
				setData:setData,
				setHorariosFalta:setHorariosFalta,
				setIdAluno:setIdAluno,
				setIdComponente:setIdComponente,
				setIdTurma:setIdTurma,
				setMotivo:setMotivo
		};

    function registrarSincronizacao() {
      sincronizacaoService.registrar('frequencia_diaria_componente',
        {
          idTurma: idTurmaSelecionada,
          idComponente: idComponenteSelecionado,
          data: dataSelecionada
        },
        'lancamento',
        null);
    }
		
		// manter ordem alfabetica
		function gravarRegistroFaltaAluno(horariosFaltaListados){
			var deferred = $q.defer();
			 angular.forEach(horariosFaltaListados, function(horarioFalta){
				prepararRegistro(horarioFalta).then(function(registroFaltaAlunoDisciplinaDiario){
					return excluirRegistroFaltaAluno(registroFaltaAlunoDisciplinaDiario);
				}).then(function(registroFaltaAlunoDisciplinaDiario){
					return inserirRegistroFaltaAluno(registroFaltaAlunoDisciplinaDiario,horarioFalta);
				}).then(function(response){
					deferred.resolve(response);
        });
			});
      registrarSincronizacao();
      return deferred.promise;
		}
		
		function gravarFalta(){
			angular.forEach(horariosFaltaListados,function(horarioFalta){
				horarioFalta.flg_fj_marcada = horarioFalta.flg_fj_marcada || false;
				horarioFalta.flg_fnj_marcada = horarioFalta.flg_fnj_marcada || false;
				horarioFalta.idMotivo = horarioFalta.idMotivo || null;
				horarioFalta.id_horario_turma = recuperarIdHorarioTurma(horarioFalta.horario_inicio, horarioFalta.horario_termino);
				horarioFalta.id_notaPeriodoAluno = null;
			});
			return gravarRegistroFaltaAluno(horariosFaltaListados).then(function(response){
        return response;
      });
		}
		
		function inserirRegistroFaltaAluno(registro, horarioFalta){
			if(horarioFalta.flg_fj_marcada || horarioFalta.flg_fnj_marcada){
        if(horarioFalta.flg_fj_marcada) {
          registro.id_motivo = motivoSelecionado;
        } else {
          registro.id_motivo = null;
        }
				return conexaoService.executar(
						inserirFaltaQuery,[
              registro.id_aluno_matricula,
              registro.id_turma,
              registro.id_disciplina,
              registro.id_nota_periodo_aluno,
              registro.data.toJSON(),
              registro.id_horario_turma,
              registro.flg_justificada,
              registro.id_motivo
        ]);
			}
		}
		
		function marcarFalta(horarioInicio, horarioFim, flg_fj_marcada, flg_fnj_marcada, idMotivo){
			angular.forEach(horariosFaltaListados,function(horarioFalta){
				if(horarioFalta.horario_inicio == horarioInicio && horarioFalta.horario_termino == horarioFim){
					horarioFalta.flg_fj_marcada = flg_fj_marcada;
					horarioFalta.flg_fnj_marcada = flg_fnj_marcada;
					horarioFalta.idMotivo = idMotivo;
					return;
				}
			});
		}
		
		function excluirRegistroFaltaAluno(registro){
			return conexaoService.executar(excluirFaltaQuery,[
          registro.id_aluno_matricula, 
          registro.id_horario_turma, 
          registro.id_disciplina, 
          formatadorService.formatatarDataSql(registro.data)
      ]).then(function(response){
				return registro;
      });
		}
		
		function prepararRegistro(horarioFalta){
			return horarioFalta.id_horario_turma.then(function(idHorarioTurma){
        var idMotivo = horarioFalta.idMotivo|| null;
				var fadd = {
					id_aluno_matricula: idAlunoSelecionado,
					id_turma:idTurmaSelecionada,
					id_disciplina:horarioFalta.id_disciplina,
					data:dataSelecionada,
					id_horario_turma:idHorarioTurma,
					flg_justificada:(horarioFalta.flg_fj_marcada)
				};
				return fadd;
      });
		}
		
		function recuperarIdHorarioTurma(horario_inicio, horarioTermino){
			return horarioTurmaService.recuperarIdHorarioTurma(horario_inicio,horarioTermino).then(function(idHorarioTurma){
				return idHorarioTurma;
      });
		}
		
		function setData(data){
      dataSelecionada = data;
		}
		
		function setHorariosFalta(horariosFalta){
			horariosFaltaListados = horariosFalta;
		}
		
		function setIdAluno(idAluno){
			idAlunoSelecionado = idAluno;
		}
		
		function setIdComponente(idComponente){
			idComponenteSelecionado = idComponente;
		}
		
		function setIdTurma(idTurma){
			idTurmaSelecionada = idTurma;
		}
		
		function setMotivo(motivo){
			motivoSelecionado = motivo;
		}
			
		return service;
	}
	
	var dataSelecionada = null;
	var horariosFaltaListados = [];
	var idAlunoSelecionado = null;
	var idComponenteSelecionado = null;
	var idTurmaSelecionada = null;
  var motivoSelecionado = null;
	
	var excluirFaltaQuery = 
		"delete from falta_aluno_disciplina_diario \r\n" + 
		"where \r\n" + 
		"id_aluno_matricula = ?\r\n" + 
		"and id_horario_turma = ?\r\n" + 
		"and id_disciplina = ?\r\n" + 
    "and date(data) = ?";
		
  var inserirFaltaQuery = 
		"insert " +
		" into falta_aluno_disciplina_diario \r\n" + 
		"	(id_aluno_matricula, id_turma, id_disciplina, id_nota_periodo_aluno, data, id_horario_turma, flg_justificada, id_motivo_falta_justificada) \r\n" + 
		" values(?, ?, ?, ?, ?, ?, ?, ?)";
})();

