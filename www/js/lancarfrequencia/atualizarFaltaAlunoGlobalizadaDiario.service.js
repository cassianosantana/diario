(function(){
	angular.module('diario.atualizarFaltaAlunoGlobalizadaDiarioService',[]).factory(
	  'atualizarFaltaAlunoGlobalizadaDiarioService',atualizarFaltaAlunoGlobalizadaDiario);
	
  atualizarFaltaAlunoGlobalizadaDiario.$inject = ['conexaoService', 'formatadorService', 'sincronizacaoService'];
	
	function atualizarFaltaAlunoGlobalizadaDiario(conexaoService, formatadorService, sincronizacaoService){
		
		var service = {
				gravarFalta:gravarFalta,
				marcarFalta:marcarFalta,
				setData:setData,
				setIdAluno:setIdAluno,
				setIdTurma:setIdTurma,
				setMotivo:setMotivo
		};

    function registrarSincronizacao() {
      sincronizacaoService.registrar('frequencia_diaria_regencia',
        {
          idTurma: idTurmaSelecionada,
          data: dataSelecionada
        },
        'lancamento',
        null);
    }
		
		function definirMotivoFaltaJustificada(isFaltaJustificada){
			if(isFaltaJustificada){
				return motivoSelecionado;
			}
      return null;
		}
		
		function excluirFalta(){
			return conexaoService.executar(excluirRegistroFaltaQuery,[
          registroFalta.id_aluno_matricula, 
          registroFalta.id_turma,
          formatadorService.formatatarDataSql(registroFalta.data)
      ]);
		}
		
		function gravarFalta(){
			if(registroFalta){
				return excluirFalta(registroFalta).then(function(response){
          return inserirFalta(registroFalta);
				}).then(function(response){
          registrarSincronizacao();
					return response;
				});
			}
		}
		
		function inserirFalta(registroFalta){
			if(registroFalta.flg_fj || registroFalta.flg_fnj){
				var idMotivoSelecionado = definirMotivoFaltaJustificada(registroFalta.flg_fj);
				registroFalta.id_motivo = idMotivoSelecionado;
				return conexaoService.executar(incluirRegistroFaltaQuery, [
            registroFalta.id_aluno_matricula,
            registroFalta.id_turma,
            null,
            !(registroFalta.flg_fnj),
            registroFalta.data.toJSON(),
            registroFalta.id_motivo
        ]);
			}
		}
		
		function marcarFalta(flg_fj, flg_fnj, motivo){
			registroFalta = {
					id_aluno_matricula:idAlunoSelecionado,
					id_turma:idTurmaSelecionada,
					data:dataSelecionada,
					flg_fj:flg_fj,
					flg_fnj:flg_fnj
			};
		}
		
		function setData(data){
			dataSelecionada = data;
		}
		
		function setIdAluno(idAluno){
			idAlunoSelecionado = idAluno;
		}
		
		function setIdTurma(idTurma){
			idTurmaSelecionada = idTurma;
		}
		
		function setMotivo(motivo){
			motivoSelecionado = motivo;
		}
		
		return service;
	}
	
	var excluirRegistroFaltaQuery =  
			"delete from \n" + 
			"falta_aluno_globalizada_diario\n" + 
			"where \n" + 
			"id_aluno_matricula = ? \n" + 
			"and id_turma =  ? \n" + 
			"and date(data) = ? ";
	
	var incluirRegistroFaltaQuery = 
		"insert into falta_aluno_globalizada_diario(\n" + 
		"	id_aluno_matricula, \n" + 
		"	id_turma, \n" + 
		"	id_nota_periodo_aluno, \n" + 
		"	flg_justificada, \n" + 
		"	data, \n" + 
		"	id_motivo_falta_justificada\n" + 
		"	) \n" + 
		"	values(? ,? ,? ,? ,? ,?)";
	
	var idAlunoSelecionado = null;
	var dataSelecionada = null;
	var idTurmaSelecionada = null;
	var motivoSelecionado = null;
	var registroFalta = null;
})();
