(function(){
	angular.module('diario.lancarFrequenciaDirective',[])
		.directive('lancarFrequenciaDirective',[lancarFrequenciaDirective])
	function lancarFrequenciaDirective(){
		
		return{
			restricted:"E",
			templateUrl:"js/lancarfrequencia/templates/lancarFrequencia.directive.html",
			scope:{
				horarioInicio:"@",
				horarioFim:"@",
				alunoSelecionado:"@",
				componenteSelecionado:"@",
				turmaSelecionada:"@",
				data:"="
			},
			controller:"horarioFaltaController",
			controllerAs:"horarioFalta",
			bindToController:true,
		}
	};
})();
	