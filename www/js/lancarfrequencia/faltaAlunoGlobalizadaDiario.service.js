(function(){
	angular.module('diario.faltaAlunoGlobalizadaDiarioService',[]).factory(
	  'faltaAlunoGlobalizadaDiarioService',faltaAlunoGlobalizadaDiarioService);
		
	faltaAlunoGlobalizadaDiarioService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];
	
	function faltaAlunoGlobalizadaDiarioService(conexaoService, conversorService,formatadorService ){
		var service = {};
		
		service.faltaAlunoData = function(idAluno, idTurma, data){
			return conexaoService.executar(alunoPossuiFaltaDataQuery,
			    [idTurma, 
			      idAluno,
			      formatadorService.formatatarDataSql(data)]).then(function(response){
				return conversorService.toArray(response.rows);
			});
    };

    service.pacoteSincronizacao = function(idTurma, data) {
      return conexaoService.executar(pacoteFrequenciaDiariaRegenciaQuery, 
          [idTurma, 
          formatadorService.formatatarDataSql(data)]).then(function(response){
        return conversorService.toArray(response.rows);
      });
    };
		
		return service;
	}
	
	var alunoPossuiFaltaDataQuery = 
		"select \n" + 
		"  id_aluno_matricula,\n" + 
		"  id_turma,\n" + 
		"  id_nota_periodo_aluno,\n" + 
		"  data,\n" + 
		"  flg_justificada,\n" + 
		"  id_motivo_falta_justificada\n" + 
		"from falta_aluno_globalizada_diario fagd \n" + 
		"where \n" + 
		"  fagd.id_turma = ?\n" + 
		"  and fagd.id_aluno_matricula = ? \n" + 
    "  and date(fagd.data) = ?";
	
	var pacoteFrequenciaDiariaRegenciaQuery = 
		"select \n" + 
		"  id_aluno_matricula,\n" + 
		"  id_turma,\n" + 
		"  id_nota_periodo_aluno,\n" + 
		"  data,\n" + 
		"  flg_justificada,\n" + 
		"  id_motivo_falta_justificada,\n" + 
		"  fagd.id_aluno_matricula \n" + 
		"from falta_aluno_globalizada_diario fagd \n" + 
		"where \n" + 
		"  fagd.id_turma = ?\n" + 
    "  and date(fagd.data) = ?";
})();
