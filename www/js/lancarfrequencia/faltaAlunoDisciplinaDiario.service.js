(function(){
	angular.module('diario.faltaAlunoDisciplinaDiarioService',[]).factory(
	  'faltaAlunoDisciplinaDiarioService', faltaAlunoDisciplinaDiarioService);
	
  faltaAlunoDisciplinaDiarioService.$inject = ['conexaoService', 'conversorService', 'componenteCurricularService', 'formatadorService'];
	
	function faltaAlunoDisciplinaDiarioService(conexaoService, conversorService, componenteCurricularService, formatadorService){
		var service = {};
		
		service.faltaAlunoHorario = function(idTurma, idComponente, data, horarioInicio, horarioTermino, idAlunoMAtricula){
			return componenteCurricularService.getDisciplinaComponenteCurricular(idComponente)
			.then(function(response){
				return response.id_disciplina;
			})
			.then(function(idDisciplina){
				return conexaoService.executar(alunoPossuiFaltaHorarioQuery, 
				  [idTurma, 
				    idDisciplina, 
				    formatadorService.formatatarDataSql(data), 
            formatadorService.formatatarHoraSql(horarioInicio), 
            formatadorService.formatatarHoraSql(horarioTermino), 
            idAlunoMAtricula]);
			})
			.then(function(response){
				return conversorService.toArray(response.rows);
			});
    };

    service.pacoteSincronizacao = function(idTurma, idComponente, data) {
			return componenteCurricularService.getDisciplinaComponenteCurricular(idComponente)
			.then(function(response){
				return response.id_disciplina;
			})
			.then(function(idDisciplina){
				return conexaoService.executar(pacoteFrequenciaDiariaComponenteQuery, 
						[idTurma, 
						 idDisciplina, 
						 formatadorService.formatatarDataSql(data)]);
			})
			.then(function(response){
				return conversorService.toArray(response.rows);
			});
    };

		return service;
	}
	
	var alunoPossuiFaltaHorarioQuery = 
		"select  \r\n" + 
		"id_falta_aluno_disciplina_diario,\r\n" + 
		"id_aluno_matricula,\r\n" + 
		"id_turma,\r\n" + 
		"id_disciplina,\r\n" + 
		"id_nota_periodo_aluno,\r\n" + 
		"data,\r\n" + 
		"flg_justificada,\r\n" + 
		"id_motivo_falta_justificada\r\n" + 
		"from falta_aluno_disciplina_diario fadd\r\n" + 
		"inner join horario_turma ht on ht.id_horario_turma = fadd.id_horario_turma\r\n" + 
		"where id_turma = ?\r\n" + 
		"and id_disciplina = ?\r\n" + 
		"and date(fadd.data) = ?\r\n" + 
		"and ht.horario_inicio = ?\r\n" + 
		"and ht.horario_termino = ?\r\n" + 
    "and fadd.id_aluno_matricula = ?";
	
	var pacoteFrequenciaDiariaComponenteQuery = 
    "select   " +
    "	id_falta_aluno_disciplina_diario, " +
    "	id_aluno_matricula, " +
    "	id_turma, " +
    "	id_disciplina, " +
    "	id_nota_periodo_aluno, " +
    "	data, " +
    "	flg_justificada, " +
    "	id_motivo_falta_justificada, " +
    "	ht.horario_inicio, " +
    "	ht.horario_termino, " +
    "	fadd.id_aluno_matricula " +
    "from falta_aluno_disciplina_diario fadd " +
    "inner join horario_turma ht on ht.id_horario_turma = fadd.id_horario_turma " +
    "where id_turma = ? " +
    "	and id_disciplina = ? " +
    "	and date(fadd.data) = ?";
})();
