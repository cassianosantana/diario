(function(){
	angular.module('diario.horarioQuadroService',[])
		.factory('horarioQuadroService',horarioQuadroService);
	
	horarioQuadroService.$inject = ['conexaoService', 'conversorService']
	
	function horarioQuadroService (conexaoService, conversorService){
		var service = {
				recuperarHorariosDiaTurmaComponente:recuperarHorariosDiaTurmaComponente,
				recuperarHorarioDiaTurmaRegencia:recuperarHorarioDiaTurmaRegencia
		};
		return service;
		
		function recuperarHorariosDiaTurmaComponente(idTurma, idComponente, diaSemana){
			return conexaoService.executar(horarioAulaDiaSemanaComponenteQuery, [idTurma, idComponente, diaSemana]);
		}
		
		function recuperarHorarioDiaTurmaRegencia(idTurma, diaSemana){
			return conexaoService.executar(horarioDiaSemanaRegenciaQuery,[idTurma, diaSemana]);
		}
	}
	
	var horarioAulaDiaSemanaComponenteQuery = 
		"select  distinct	\r\n" + 
    "strftime('%H:%M', h.horario_inicio) as horario_inicio,\r\n" + 
    "strftime('%H:%M', h.horario_termino) as horario_termino,\r\n" + 
		"q.id_componente_curricular,\r\n" + 
		"cc.id_disciplina,\r\n" + 
		"ds.dia_semana\r\n" + 
		"	from turma_quadro_homologado t\r\n" + 
		"		inner join quadro_horario_homologado q on t.id_turma_quadro_homologado= q.id_turma_quadro_homologado\r\n" + 
		"		inner join dia_semana ds on ds.dia_semana = q.dia_semana\r\n" + 
		"		inner join componente_curricular cc on cc.id_componente_curricular = q.id_componente_curricular\r\n" + 
		"		inner join horario_quadro_homologado h on h.id_horario_quadro_homologado = q.id_horario_quadro_homologado\r\n" + 
		"	where \r\n" + 
		"		t.id_turma = ?	\r\n" + 
		"		and q.id_componente_curricular = ? \r\n" + 
		"		and ds.dia_semana_postgresql = ? \r\n" + 
		"order by horario_inicio";
	
	var horarioDiaSemanaRegenciaQuery =
		"select \r\n" + 
		" strftime('%H:%M', min(t.horario_inicio)) as horario_inicio,\r\n" + 
		" strftime('%H:%M', max(t.horario_termino)) as horario_termino \r\n" + 
		"from (\r\n" + 
		"select\r\n" + 
		"h.horario_inicio ,\r\n" + 
		"h.horario_termino ,\r\n" + 
		"q.id_componente_curricular,\r\n" + 
		"cc.id_disciplina,\r\n" + 
		"ds.dia_semana\r\n" + 
		"	from turma_quadro_homologado t\r\n" + 
		"		inner join quadro_horario_homologado q on t.id_turma_quadro_homologado= q.id_turma_quadro_homologado\r\n" + 
		"		inner join dia_semana ds on ds.dia_semana = q.dia_semana\r\n" + 
		"		inner join componente_curricular cc on cc.id_componente_curricular = q.id_componente_curricular\r\n" + 
		"		inner join horario_quadro_homologado h on h.id_horario_quadro_homologado = q.id_horario_quadro_homologado\r\n" + 
		"	where \r\n" + 
		"		t.id_turma = ?	\r\n" + 
		"		and ds.dia_semana_postgresql = ? \r\n" + 
		"order by horario_inicio\r\n" + 
		") t\r\n" + 
		"\r\n" + 
		""
})();
