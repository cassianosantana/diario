(function(){
	angular.module('diario.horarioFaltaService',[])
		.factory('horarioFaltaService',horarioFaltaService);
		
	horarioFaltaService.$inject = ['$q','conexaoService']
		
	function horarioFaltaService($q, conexaoService){
		// constantes um excelente uso para services
		
		
		var service = {
			getMotivosFaltaJustificada:getMotivosFaltaJustificada,
			ID_FALTA_JUSTIFICADA:FALTA_JUSTIFICADA,
			ID_FALTA_NAO_JUSTIFICADA:FALTA_NAO_JUSTIFICADA
		}
		
		return service;
		
		function getMotivosFaltaJustificada(){
			return conexaoService.executar(motivoFaltaJustificadaQuery);
		}
	}
	
	var data = null
	var FALTA_JUSTIFICADA = "fj";
	var FALTA_NAO_JUSTIFICADA = "fnj";
		
	var motivoFaltaJustificadaQuery = 
		"select \r\n" + 
		"id_motivo_falta_justificada,\r\n" + 
		"descricao \r\n" + 
		"from \r\n" + 
		"motivo_falta_justificada ;"
})();