(function(){
  angular.module('diario.registroFaltaDisciplinaDiarioService',[])
    .factory('registroFaltaDisciplinaDiarioService',registroFaltaDisciplinaDiarioService);


  registroFaltaDisciplinaDiarioService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];

  function registroFaltaDisciplinaDiarioService(conexaoService, conversorService, formatadorService){
    var service = {
    		isRegistroDiarioConcluido:isRegistroDiarioConcluido

    };

    return service;

    function isRegistroDiarioConcluido(idTurma, idDisciplina, dataReferencia){
      return conexaoService.executar(isRegistroDiarioConcluidoQuery,[idTurma, idDisciplina, formatadorService.formatatarDataSql(dataReferencia)])
      .then(function(response){
        if(response.rows.length == 0){
          return {
            flg_concluido:false
          };
        }
        return conversorService.toObject(response.rows);
      })
    }
  }
var isRegistroDiarioConcluidoQuery =
"select \n" +
"rfdd.flg_concluido \n " +
"from \n" +
"registro_falta_disciplina_diario rfdd \n" +
"where \n" +
" rfdd.id_turma = ? \n" +
" and rfdd.id_disciplina = ? \n" +
" and date(rfdd.data_referencia) = ? "

})();
