(function(){
	angular.module('diario.lancarAlunoFrequenciaController',[])
		.controller('lancarAlunoFrequenciaController',lancarAlunoFrequenciaController);

	lancarAlunoFrequenciaController.$inject = ['$state',
	                                           '$ionicViewSwitcher',
	                                           '$ionicSideMenuDelegate',
	                                           'lancarAlunoFrequenciaService',
	                                           '$ionicModal',
	                                           '$scope',
	                                           'conversorService',
	                                           'componenteCurricularService',
	                                           'listarTurmaService',
	                                           'horarioQuadroService',
	                                           'atualizarFaltaAlunoDisciplinaDiarioService',
	                                           'atualizarFaltaAlunoGlobalizadaDiarioService',
	                                           'registroFaltaDisciplinaDiarioService',
																						 'registroFaltaGlobalDiarioService',
																						 'atualizarRegistroFaltaDisciplinaDiarioService',
																						 'atualizarRegistroFaltaGlobalDiarioService'
	                                           ]


	function lancarAlunoFrequenciaController($state, $ionicViewSwitcher,
			$ionicSideMenuDelegate, lancarAlunoFrequenciaService, $ionicModal, $scope,
			conversorService, componenteCurricularService, listarTurmaService, horarioQuadroService,
			atualizarFaltaAlunoDisciplinaDiarioService, atualizarFaltaAlunoGlobalizadaDiarioService,
			registroFaltaDisciplinaDiarioService, registroFaltaGlobalDiarioService, atualizarRegistroFaltaDisciplinaDiarioService, atualizarRegistroFaltaGlobalDiarioService){
		var ctx = this;

		//binds do modelo listar aluno
		// manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.apontamentoConcluido = isApontamentoConcluido($state.params.idTurma, $state.params.idComponente, new Date($state.params.data))
		listarAlunos(new Date($state.params.data), $state.params.idComponente, $state.params.idTurma);
		ctx.concluirApontamento = concluirApontamento;
		ctx.dataSelecionada = new Date($state.params.data);
		ctx.isMenuAberto = isMenuAberto;
		ctx.lancarFrequencia = lancarFrequencia;
		ctx.marcarFaltaJustificada = marcarFaltaJustificada;
		ctx.dadosTurma = {};
		ctx.dadosTurma.idTurma = $state.params.idTurma;
		ctx.dadosTurma.idComponente = $state.params.idComponente;
		ctx.dadosTurma.diaSemana = new Date($state.params.data).getDay();
		ctx.dadosTurma.data = new Date($state.params.data);
		ctx.verificarFalta = verificarFalta;
		ctx.voltar = voltar;

		// implementacoes dos binds do modelo listar aluno
		// manter ordem alfabetica

		// abre o menu direito
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}

		// marca o apontamento diario como concluido
		function concluirApontamento(isApontamentoConcluido){
			if(isApontamentoConcluido){
				var modal = $ionicModal.fromTemplateUrl("js/compartilhado/modaltemplates/modalDetalharTemplate.html",{
					scope: $scope,
				    animation: 'slide-in-up'
				})
				.then(function(data){
					 $scope.modal = data;
					 $scope.modal.show();
				})
			}
			return listarTurmaService.isTurmaRegenciaClasse($state.params.idTurma)
			.then(function(isTurmaRegenciaClasse){
				if(isTurmaRegenciaClasse === "true"){
					return gravarApontamentoConcluidoTurmaRegencia($state.params.idTurma, new Date($state.params.data));
				}else{
					return gravarApontamentoConcluidoTurmaComponente($state.params.idTurma, $state.params.idComponente, new Date($state.params.data));
				}
			})
		}

		function gravarApontamentoConcluidoTurmaComponente(idTurma, idComponente, dataReferencia){
			var disciplinaSelencionada = null;
				return componenteCurricularService.getDisciplinaComponenteCurricular(idComponente)
			.then(function(disciplina){
				disciplinaSelencionada = disciplina.id_disciplina
				return atualizarRegistroFaltaDisciplinaDiarioService.recuperarApontamentoAnterior(idTurma,disciplinaSelencionada , dataReferencia)
			})
			.then(function(apontamentoAnterior){
				if(!apontamentoAnterior){
					prepararInclusaoConcluirApontamentoTurmaComponente(idTurma, disciplinaSelencionada, dataReferencia, ctx.apontamentoConcluido);
					return atualizarRegistroFaltaDisciplinaDiarioService.inserirApontamentoConcluido()
				}else{
					prepararAlteracaoConcluirApontamentoTurmaComponente(apontamentoAnterior,ctx.apontamentoConcluido);
					return atualizarRegistroFaltaDisciplinaDiarioService.atualizarApontamentoConcluido();
				}
			})
		}

		function gravarApontamentoConcluidoTurmaRegencia(idTurma, dataReferencia){
			return atualizarRegistroFaltaGlobalDiarioService.recuperarApontamentoAnterior(idTurma, dataReferencia)
			.then(function(apontamentoAnterior){
				if(!apontamentoAnterior){
					prepararInclusaoConcluirApontamentoTurmaRegencia(idTurma, dataReferencia, ctx.apontamentoConcluido);
					return atualizarRegistroFaltaGlobalDiarioService.inserirApontamentoConcluido();
				}else{
					prepararAlteracaoConcluirApontamentoTurmaRegencia(apontamentoAnterior, ctx.apontamentoConcluido);
					return atualizarRegistroFaltaGlobalDiarioService.atualizarApontamentoConcluido();
				}
			})
		}

		function isApontamentoConcluido(idTurma, idComponente, data){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(isTurmaRegencia){
				if(isTurmaRegencia === "true"){
					return isApontamentoConcluidoRegencia(idTurma, data)
				}else{
					return isApontamentoConcluidoComponente(idTurma, idComponente, data)
				}
			})
		}

		function isApontamentoConcluidoComponente(idTurma, idComponente, data){
			return componenteCurricularService.getDisciplinaComponenteCurricular(idComponente)
			.then(function(response){
				return response.id_disciplina;
			})
			.then(function(idDisciplina){
				return registroFaltaDisciplinaDiarioService.isRegistroDiarioConcluido(idTurma, idDisciplina, data)
			})
			.then(function(apontamento){
				return ctx.apontamentoConcluido = apontamento.flg_concluido === "true";
			})
		}

		function isApontamentoConcluidoRegencia(idTurma, data){
			return registroFaltaGlobalDiarioService.isRegistroDiarioConcluido(idTurma,data)
			.then(function(apontamento){
				return ctx.apontamentoConcluido = apontamento.flg_concluido === "true";
			})
		}

		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}

		function lancarFrequencia (idAluno){
			$ionicViewSwitcher.nextDirection('forward');
			$state.go("lancarFrequencia",{
				"idUnidade":$state.params.idUnidade,
				"idTurma":$state.params.idTurma,
				"idComponente":$state.params.idComponente,
				"data":$state.params.data,
				"idAluno":idAluno
			});
		}

		function listarAlunos(data, idComponente, idTurma){
			return listarTurmaService.isTurmaRegenciaClasse(idTurma)
			.then(function(isTurmaRegencia){
				if(isTurmaRegencia === "true"){
					return listarAlunosTurmaRegencia(data, idTurma);
				}else{
					return listarAlunosTurmaComponente(data, idComponente, idTurma)
				}
			});
		}

		function listarAlunosTurmaComponente(data, idComponente, idTurma){
			return componenteCurricularService.getDisciplinaComponenteCurricular(idComponente)
				.then(function(response){
					return response.id_disciplina;
				})
				.then(function(idDisciplina){
					return  lancarAlunoFrequenciaService.getAlunosTurmaComponente(data, idDisciplina, idTurma)
				})
				.then(function(response){
					return ctx.alunos = conversorService.toArray(response.rows);
				})
		}

		function listarAlunosTurmaRegencia(data, idTurma){
			return lancarAlunoFrequenciaService.getAlunosTurmaRegencia(data, idTurma)
			.then(function(response){
				return ctx.alunos = conversorService.toArray(response.rows);
			});
		}

		function marcarFaltaJustificada(dadosTurma, dadosAluno){
			return  listarTurmaService.isTurmaRegenciaClasse(dadosTurma.idTurma)
				.then(function(response){
					if(response === "true"){
						return marcarFaltaJustificadaTurmaRegencia(dadosTurma, dadosAluno);
					}else{
						return marcarFaltaJustificadaTurmaComponente(dadosTurma, dadosAluno);
					}
				})

		}

		function marcarFaltaJustificadaTurmaComponente(dadosTurma, dadosAluno){
			return horarioQuadroService.recuperarHorariosDiaTurmaComponente(dadosTurma.idTurma, dadosTurma.idComponente, dadosTurma.diaSemana)
			.then(function(response){
				return conversorService.toArray(response.rows)
			})
			.then(function(horarios){
				angular.forEach(horarios,function(horario){
					horario.flg_fnj_marcada = dadosAluno.flg_possui_falta == 0;
					horario.data = dadosTurma.data;
				});
				dadosAluno.flg_possui_falta = (dadosAluno.flg_possui_falta != 0 ? 0 : 1 );
				atualizarFaltaAlunoDisciplinaDiarioService.setHorariosFalta(horarios);
				atualizarFaltaAlunoDisciplinaDiarioService.setIdAluno(dadosAluno.id_aluno_matricula);
				atualizarFaltaAlunoDisciplinaDiarioService.setIdComponente(dadosTurma.idComponente);
				atualizarFaltaAlunoDisciplinaDiarioService.setIdTurma(dadosTurma.idTurma);
				atualizarFaltaAlunoDisciplinaDiarioService.setData(dadosTurma.data);

				 return atualizarFaltaAlunoDisciplinaDiarioService.gravarFalta();
			})
		}

		function marcarFaltaJustificadaTurmaRegencia(dadosTurma, dadosAluno){
			atualizarFaltaAlunoGlobalizadaDiarioService.setIdTurma(dadosTurma.idTurma);
			atualizarFaltaAlunoGlobalizadaDiarioService.setIdAluno(dadosAluno.id_aluno_matricula);
			atualizarFaltaAlunoGlobalizadaDiarioService.setData(dadosTurma.data);
			atualizarFaltaAlunoGlobalizadaDiarioService.marcarFalta(false,dadosAluno.flg_possui_falta == 0,null);
			dadosAluno.flg_possui_falta = (dadosAluno.flg_possui_falta != 0 ? 0 : 1 );
				return atualizarFaltaAlunoGlobalizadaDiarioService.gravarFalta();
		}


		function prepararAlteracaoConcluirApontamentoTurmaComponente(apontamentoAnterior, flgApontamentoConcluido){
			atualizarRegistroFaltaDisciplinaDiarioService.setIdTurma(apontamentoAnterior.id_turma);
			atualizarRegistroFaltaDisciplinaDiarioService.setIdDisciplina(apontamentoAnterior.id_disciplina);
			atualizarRegistroFaltaDisciplinaDiarioService.setDataAtualizacao(new Date());
			atualizarRegistroFaltaDisciplinaDiarioService.setDataInclusao(new Date(apontamentoAnterior.data_inclusao));
			atualizarRegistroFaltaDisciplinaDiarioService.setDataReferencia(new Date(apontamentoAnterior.data_referencia));
			atualizarRegistroFaltaDisciplinaDiarioService.setflgConcluido(flgApontamentoConcluido);
			return;
		}
		function prepararAlteracaoConcluirApontamentoTurmaRegencia(apontamentoAnterior, flgApontamentoConcluido){
			atualizarRegistroFaltaGlobalDiarioService.setIdTurma(apontamentoAnterior.id_turma);
			atualizarRegistroFaltaGlobalDiarioService.setDataReferencia(new Date(apontamentoAnterior.data_referencia));
			atualizarRegistroFaltaGlobalDiarioService.setDataInclusao(new Date(apontamentoAnterior.data_inclusao));
			atualizarRegistroFaltaGlobalDiarioService.setDataAtualizacao(new Date());
			atualizarRegistroFaltaGlobalDiarioService.setflgConcluido(flgApontamentoConcluido);
			return;
		}

		function prepararInclusaoConcluirApontamentoTurmaComponente(idTurma, idDisciplina, dataReferencia, flgApontamentoConcluido){
			atualizarRegistroFaltaDisciplinaDiarioService.setIdTurma(idTurma);
			atualizarRegistroFaltaDisciplinaDiarioService.setIdDisciplina(idDisciplina);
			atualizarRegistroFaltaDisciplinaDiarioService.setDataReferencia(new Date(dataReferencia));
			atualizarRegistroFaltaDisciplinaDiarioService.setDataInclusao(new Date());
			atualizarRegistroFaltaDisciplinaDiarioService.setDataAtualizacao(new Date());
			atualizarRegistroFaltaDisciplinaDiarioService.setflgConcluido(flgApontamentoConcluido);
			return;
		}

		function prepararInclusaoConcluirApontamentoTurmaRegencia(idTurma, dataReferencia,flgApontamentoConcluido){
			atualizarRegistroFaltaGlobalDiarioService.setIdTurma(idTurma);
			atualizarRegistroFaltaGlobalDiarioService.setDataInclusao(new Date());
			atualizarRegistroFaltaGlobalDiarioService.setDataAtualizacao(new Date());
			atualizarRegistroFaltaGlobalDiarioService.setDataReferencia(dataReferencia);
			atualizarRegistroFaltaGlobalDiarioService.setflgConcluido(flgApontamentoConcluido);
			return; 
		}

		function verificarFalta(possuiFalta){
			return possuiFalta == 0;
		}

		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		}

	}

})();
