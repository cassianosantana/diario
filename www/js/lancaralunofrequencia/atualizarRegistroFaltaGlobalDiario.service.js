(function(){
	angular.module('diario.atualizarRegistroFaltaGlobalDiarioService',[])
		.factory('atualizarRegistroFaltaGlobalDiarioService',atualizarRegistroFaltaGlobalDiarioService);

	atualizarRegistroFaltaGlobalDiarioService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];

	function atualizarRegistroFaltaGlobalDiarioService(conexaoService, conversorService, formatadorService){
		var service = {
			atualizarApontamentoConcluido:atualizarApontamentoConcluido,
			getDataAtualizacao:getDataAtualizacao,
			getDataInclusao:getDataInclusao,
			getDataReferencia:getDataReferencia,
			getFlgConcluido:getFlgConcluido,
			getIdTurma:getIdTurma,
			inserirApontamentoConcluido:inserirApontamentoConcluido,
			recuperarApontamentoAnterior:recuperarApontamentoAnterior,
			setDataAtualizacao:setDataAtualizacao,
			setDataInclusao:setDataInclusao,
			setDataReferencia:setDataReferencia,
			setflgConcluido:setflgConcluido,
			setIdTurma:setIdTurma,
		};
		return service;

		function atualizarApontamentoConcluido(){
			return conexaoService.executar(atualizarApontamentoConcluidoQuery,[
																																					registroFaltaGlobalDiario.id_turma,
																																					registroFaltaGlobalDiario.data_inclusao.toJSON(),
																																					registroFaltaGlobalDiario.data_atualizacao.toJSON(),
																																					registroFaltaGlobalDiario.data_referencia.toJSON(),
																																					registroFaltaGlobalDiario.flg_concluido,
																																					registroFaltaGlobalDiario.id_turma,
																																					formatadorService.formatatarDataSql(registroFaltaGlobalDiario.data_referencia)
																																				])
		}

		function getIdTurma(){
			return registroFaltaGlobalDiario.id_turma;
		}

		function getDataInclusao(){
			return registroFaltaGlobalDiario.data_inclusao;
		}

		function getDataAtualizacao(){
			return registroFaltaGlobalDiario.data_atualizacao;
		}

		function getDataReferencia(){
			return registroFaltaGlobalDiario.data_referencia
		}

		function getFlgConcluido(){
			return registroFaltaGlobalDiario.flg_concluido;
		}

		function inserirApontamentoConcluido(){
			return conexaoService.executar(inserirApontamentoConcluidoQuery,[
																																				registroFaltaGlobalDiario.id_turma,
																																			  registroFaltaGlobalDiario.data_inclusao.toJSON(),
																																				registroFaltaGlobalDiario.data_atualizacao.toJSON(),
																																				registroFaltaGlobalDiario.data_referencia.toJSON(),
																																				registroFaltaGlobalDiario.flg_concluido
																																			])
		}

		function recuperarApontamentoAnterior(idTurma, data){
			return conexaoService.executar(apontamentoAnteriorQuery,[idTurma,formatadorService.formatatarDataSql(data)])
			.then(function(response){
				if(response.rows.length == 0){
					return null;
				}
				return conversorService.toObject(response.rows);
			})
		}

		function setDataAtualizacao(data){
			registroFaltaGlobalDiario.data_atualizacao = data;
		}

		function setDataInclusao(data){
			registroFaltaGlobalDiario. data_inclusao = data;
		}

		function setDataReferencia(data){
			registroFaltaGlobalDiario.data_referencia = data;
		}

		function setflgConcluido(flgConcluido){
			registroFaltaGlobalDiario.flg_concluido = flgConcluido;
		}

		function setIdTurma(idTurma){
			registroFaltaGlobalDiario.id_turma = idTurma;
		}
	}

var apontamentoAnteriorQuery =
"select \n" +
"rfgd.id_turma,\n" +
"rfgd.data_inclusao,\n" +
"rfgd.data_atualizacao,\n" +
"rfgd.data_referencia,\n" +
"rfgd.flg_concluido \n" +
"from \n" +
"registro_falta_global_diario rfgd \n" +
"    where rfgd.id_turma = ? \n" +
"		 and date(rfgd.data_referencia) = ?\n";


var atualizarApontamentoConcluidoQuery =
"update \n" +
"registro_falta_global_diario\n" +
"set \n" +
"    id_turma = ?,\n" +
"    data_inclusao = ?,\n" +
"    data_atualizacao = ?, \n" +
"    data_referencia = ?, \n" +
"    flg_concluido = ?\n" +
"where \n" +
"    id_turma = ?\n" +
"    and date(data_referencia) = ?";

var inserirApontamentoConcluidoQuery =
"insert into registro_falta_global_diario(\n" +
"    id_turma, \n" +
"    data_inclusao,\n" +
"    data_atualizacao,\n" +
"    data_referencia,\n" +
"    flg_concluido\n" +
") values(?, ?, ?, ?, ?)";

	var registroFaltaGlobalDiario = {
		id_turma:null,
		data_inclusao:null,
		data_atualizacao:null,
		data_referencia:null,
		flg_concluido:null
	};
})();
