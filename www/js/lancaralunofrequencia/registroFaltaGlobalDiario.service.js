(function(){
	angular.module('diario.registroFaltaGlobalDiarioService',[])
		.factory('registroFaltaGlobalDiarioService',registroFaltaGlobalDiarioService);

	registroFaltaGlobalDiarioService.$inject = ['conexaoService', 'conversorService', 'formatadorService'];


	function registroFaltaGlobalDiarioService(conexaoService, conversorService,formatadorService){
		var service = {
				isRegistroDiarioConcluido:isRegistroDiarioConcluido
		};

		return service;

		function isRegistroDiarioConcluido(idTurma, dataReferencia){
			return conexaoService.executar(isRegistroDiarioConcluidoQuery,[idTurma, formatadorService.formatatarDataSql(dataReferencia)])
			.then(function(response){
				if(response.rows.length == 0){
					return{
						flg_concluido:false
					};
				}
				return conversorService.toObject(response.rows)
			})
		}
	}

	var isRegistroDiarioConcluidoQuery =
		"select \n" +
		"rfgd.flg_concluido\n" +
		"from \n" +
		"registro_falta_global_diario rfgd\n" +
		"where \n" +
		"rfgd.id_turma = ?\n" +
		"and date(rfgd.data_referencia) = ?";

})();
