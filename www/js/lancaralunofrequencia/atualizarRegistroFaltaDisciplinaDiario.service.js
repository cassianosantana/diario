(function(){
	angular.module('diario.atualizarRegistroFaltaDisciplinaDiarioService',[])
		.factory('atualizarRegistroFaltaDisciplinaDiarioService',atualizarRegistroFaltaDisciplinaDiarioService)

		atualizarRegistroFaltaDisciplinaDiarioService.$inject = [
																														'conexaoService',
																														'conversorService',
																														'registroFaltaDisciplinaDiarioService',
																														'componenteCurricularService',
																														'formatadorService'
																													];

		function atualizarRegistroFaltaDisciplinaDiarioService(conexaoService,
			conversorService,registroFaltaDisciplinaDiarioService, componenteCurricularService, formatadorService){
			var service = {
				atualizarApontamentoConcluido:atualizarApontamentoConcluido,
				getDataAtualizacao:getDataAtualizacao,
				getDataInclusao:getDataInclusao,
				getDataReferencia:getDataReferencia,
				getIdDisciplina:getIdDisciplina,
				getIdTurma:getIdTurma,
				inserirApontamentoConcluido:inserirApontamentoConcluido,
				recuperarApontamentoAnterior:recuperarApontamentoAnterior,
				setDataAtualizacao:setDataAtualizacao,
				setDataInclusao:setDataInclusao,
				setDataReferencia:setDataReferencia,
				setflgConcluido:setflgConcluido,
				setIdDisciplina:setIdDisciplina,
				setIdTurma:setIdTurma

			};

			return service;


			function atualizarApontamentoConcluido(){
				return conexaoService.executar(atualizarApontamentoConcluidoQuery,[
					registroFaltaDisciplinaDiario.id_turma,
					registroFaltaDisciplinaDiario.id_disciplina,
					registroFaltaDisciplinaDiario.data_inclusao.toJSON(),
					registroFaltaDisciplinaDiario.data_atualizacao.toJSON(),
					registroFaltaDisciplinaDiario.data_referencia.toJSON(),
					registroFaltaDisciplinaDiario.flg_concluido,
					registroFaltaDisciplinaDiario.id_turma,
					registroFaltaDisciplinaDiario.id_disciplina,
					formatadorService.formatatarDataSql(registroFaltaDisciplinaDiario.data_referencia)

				])
			}

			function inserirApontamentoConcluido(){
				return conexaoService.executar(inserirApontamentoConcluidoQuery,[
					registroFaltaDisciplinaDiario.id_turma,
					registroFaltaDisciplinaDiario.id_disciplina,
					registroFaltaDisciplinaDiario.data_inclusao.toJSON(),
					registroFaltaDisciplinaDiario.data_atualizacao.toJSON(),
					registroFaltaDisciplinaDiario.data_referencia.toJSON(),
					registroFaltaDisciplinaDiario.flg_concluido
				])
			}

			function recuperarApontamentoAnterior(idTurma, idDisciplina, data){

					return conexaoService.executar(apontamentoAnteriorQuery, [idTurma, idDisciplina,formatadorService.formatatarDataSql(data)])
				.then(function(response){
					if(response.rows.length == 0){
						return null;
					}
					return conversorService.toObject(response.rows);
				})
			}

			function getDataAtualizacao(){
				return registroFaltaDisciplinaDiario.data_atualizacao
			}

			function getDataInclusao(){
				return registroFaltaDisciplinaDiario.data_inclusao;
			}

			function getDataReferencia(){
				return registroFaltaDisciplinaDiario.data_referencia;
			}

			function getFlgConcluido(){
				return registroFaltaDisciplinaDiario.flg_concluido;
			}

			function getIdDisciplina(){
				return registroFaltaDisciplinaDiario.id_disciplina;
			}

			function getIdTurma(){
				return registroFaltaDisciplinaDiario.id_turma;
			}

			function setDataInclusao(data){
				registroFaltaDisciplinaDiario.data_inclusao = data;
			}

			function setDataAtualizacao(data){
				registroFaltaDisciplinaDiario.data_atualizacao = data;
			}

			function setDataReferencia(data){
				registroFaltaDisciplinaDiario.data_referencia = data;
			}

			function setflgConcluido(flgConvluido){
				registroFaltaDisciplinaDiario.flg_concluido = flgConvluido;
			}

			function setIdDisciplina(idDisciplina){
				registroFaltaDisciplinaDiario.id_disciplina = idDisciplina;
			}

			function setIdTurma(idTurma){
				registroFaltaDisciplinaDiario.id_turma = idTurma;
			}

		}

		var registroFaltaDisciplinaDiario = {
			id_turma:null,
			idDisciplina:null,
			data_inclusao:null,
			data_atualizacao:null,
			data_referencia:null,
			flg_concluido:null
		};

		var apontamentoAnteriorQuery =
		"select\n" +
		"rfdd.id_turma,\n" +
		"rfdd.id_disciplina,\n" +
		"rfdd.flg_concluido,\n" +
		"rfdd.data_atualizacao, \n" +
		"rfdd.data_referencia \n " +
		"from registro_falta_disciplina_diario rfdd\n" +
		"where rfdd.id_turma = ? \n" +
		"and rfdd.id_disciplina = ?\n" +
		"and date(rfdd.data_referencia) = ?\n"

  var inserirApontamentoConcluidoQuery =
	"insert into registro_falta_disciplina_diario(\n" +
	"id_turma, \n" +
	"id_disciplina, \n" +
	"data_inclusao, \n" +
	"data_atualizacao, \n" +
	"data_referencia, \n" +
	"flg_concluido) values(?, ?, ?, ?, ?, ?)";

	var atualizarApontamentoConcluidoQuery =
	"update \n" +
	"registro_falta_disciplina_diario \n" +
	"set\n" +
	"id_turma = ?,\n" +
	"id_disciplina = ?,\n" +
	"data_inclusao = ?,\n" +
	"data_atualizacao = ?,\n" +
	"data_referencia = ?,\n" +
	"flg_concluido = ?\n" +
	"where \n" +
	"    id_turma = ?\n" +
	"    and id_disciplina = ?\n" +
	"    and date(data_referencia) = ? "

})();
