(function(){
	angular.module('diario.lancarAlunoFrequenciaService',[])
		.factory('lancarAlunoFrequenciaService',lancarAlunoFrequenciaService);
	
	lancarAlunoFrequenciaService.$inject = ['conexaoService', 'formatadorService'];
	
	function lancarAlunoFrequenciaService(conexaoService, formatadorService){
		var service = {
				
			getAluno:getAluno,	
			getAlunosTurmaComponente:getAlunosTurmaComponente,
			getAlunosTurmaRegencia:getAlunosTurmaRegencia
		};
		return service;
		
		// recupera um aluno com base no idAlunoMAtricula
		function getAluno(idAlunoMatricula){
			return conexaoService.executar(selecionarQuery,[idAlunoMatricula]);
		};
		// recupera a lista de alunoa baseado na turma do professor
		function getAlunosTurmaComponente(data, disciplina, id_turma){
			return conexaoService.executar(listarFaltaComponenteQueryOtimizada,[formatadorService.formatatarDataSql(data),disciplina, id_turma]);
		}
		
		function getAlunosTurmaRegencia(data, idTurma){
			return conexaoService.executar(listrFaltaTurmaRegenciaQuery,[formatadorService.formatatarDataSql(data), idTurma]);
		}
		
	}
	
	
	// custo 165.. 168
	var listarFaltaComponenteQueryOtimizada = 
		"\r\n" + 
		"select \r\n" + 
		"am.id_aluno_matricula,\r\n" + 
		"case when falta_aluno.id_aluno_matricula is not null then 1 else 0 end as flg_possui_falta,\r\n" + 
		"cid.nome,\r\n" + 
		"amt.chamada\r\n" + 
		"from \r\n" + 
		"cidadao cid\r\n" + 
		"	inner join aluno_matricula am on am.id_cidadao = cid.id_cidadao\r\n" + 
		"	inner join aluno_matr_turma amt on amt.id_aluno_matricula = am.id_aluno_matricula\r\n" + 
		"	inner join turma t on t.id_turma = amt.id_turma \r\n" + 
		"	inner join grade_curricular gc on gc.id_grade_curricular = t.id_grade_curricular\r\n" + 
		"	inner join componente_curricular cc on cc.id_grade_curricular  = gc.id_grade_curricular\r\n" + 
		"	inner join disciplina d on d.id_disciplina = cc.id_disciplina \r\n" + 
		"	left join (\r\n" + 
		"		select \r\n" + 
		"		fadd.id_falta_aluno_disciplina_diario,\r\n" + 
		"		fadd.data,\r\n" + 
		"		fadd.id_turma,\r\n" + 
		"		fadd.id_disciplina,\r\n" + 
		"		fadd.id_aluno_matricula\r\n" + 
		"		from falta_aluno_disciplina_diario fadd\r\n" + 
		"	) falta_aluno on falta_aluno.id_disciplina = d.id_disciplina and falta_aluno.id_turma = t.id_turma and falta_aluno.id_aluno_matricula = am.id_aluno_matricula and date(falta_aluno.data) = ?\r\n" + 
		"where\r\n" + 
		" d.id_disciplina =  ?\r\n" + 
		"and t.id_turma = ?\r\n" + 
		"and am.id_aluno_matr_status = 1\r\n" + 
		"group by \r\n" + 
		"am.id_aluno_matricula,\r\n" + 
		"cid.nome,\r\n" + 
		"falta_aluno.id_aluno_matricula, \r\n" + 
		"amt.chamada\r\n" + 
		"order by amt.chamada";
		
	// custo 465.. 472
	var listarFaltaTurmaComponenteQuery = 
		"select am.id_aluno_matricula, \r\n" + 
		"cid.nome,\r\n" + 
		"amt.chamada,\r\n" + 
		" case when count (fadd.id_falta_aluno_disciplina_diario) > 0 then 1 else 0 end as flg_possui_falta   \r\n" + 
		"from cidadao cid \r\n" + 
		"	inner join aluno_matricula am on am.id_cidadao = cid.id_cidadao \r\n" + 
		"	inner join aluno_matr_turma amt on am.id_aluno_matricula = amt.id_aluno_matricula \r\n" + 
		"	inner join turma t on t.id_turma = amt.id_turma\r\n" + 
		"	inner join grade_curricular gc on gc.id_grade_curricular = t.id_grade_curricular\r\n" + 
		"	left join falta_aluno_disciplina_diario fadd on fadd.id_aluno_matricula = am.id_aluno_matricula and fadd.id_turma = t.id_turma and date(fadd.data) = ? and fadd.id_disciplina = ? \r\n" + 
		"	left join componente_curricular cc on cc.id_grade_curricular = gc.id_grade_curricular and cc.id_disciplina = fadd.id_disciplina \r\n" + 
		"where\r\n" + 
		"	t.id_turma = ? \r\n" + 
		"	and am.id_aluno_matr_status = 1\r\n" + 
		"group by \r\n" + 
		"	am.id_aluno_matricula,\r\n" + 
		"	cid.nome,\r\n" + 
		"	amt.chamada\r\n" + 
		"order by \r\n" + 
		"	amt.chamada \r\n" + 
		""
	
	var listrFaltaTurmaRegenciaQuery = 
		"select \r\n" + 
		"am.id_aluno_matricula,\r\n" +
		" cid.nome, " + 
		"amt.chamada,\r\n" + 
		"case when \r\n" + 
		"	falta_aluno.id_aluno_matricula is not null then 1 else 0\r\n" + 
		"end as flg_possui_falta\r\n" + 
		"from cidadao cid \r\n" + 
		"inner join aluno_matricula am on am.id_cidadao = cid.id_cidadao\r\n" + 
		"inner join aluno_matr_turma amt on am.id_aluno_matricula = amt.id_aluno_matricula\r\n" + 
		"inner join turma t on t.id_turma = amt.id_turma\r\n" + 
		"left join(\r\n" + 
		"		select \r\n" + 
		"			fagd.id_aluno_matricula,\r\n" + 
		"			fagd.id_turma,\r\n" + 
		"			fagd.data\r\n" + 
		"		from \r\n" + 
		"		falta_aluno_globalizada_diario fagd\r\n" + 
		"\r\n" + 
		"	)falta_aluno on falta_aluno.id_turma = t.id_turma and falta_aluno.id_aluno_matricula = am.id_aluno_matricula and date(falta_aluno.data) = ?		\r\n" + 
		"	\r\n" + 
		"where \r\n" + 
		"t.id_turma = ?\r\n" +
		"and am.id_aluno_matr_status = 1 \n" + 
		"\r\n" + 
		"order by amt.chamada\r\n" + 
		"";
		
	var selecionarQuery = 
		"select  \r\n" + 
		"cid.nome, \r\n" + 
		"a.codigo  \r\n" + 
		"from cidadao cid\r\n" + 
		"inner join aluno a on a.id_cidadao = cid.id_cidadao  \r\n" + 
		"inner join aluno_matricula am on am.id_cidadao = cid.id_cidadao  \r\n" + 
		"where \r\n" + 
		"am.id_aluno_matricula = ?";
	
	
})();