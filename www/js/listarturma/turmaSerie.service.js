(function(){
	angular.module('diario.turmaSerieService',[])
		.factory('turmaSerieService',turmaSerieService);

	turmaSerieService.$inject = ['conexaoService', 'conversorService', 'turmaMultiEtapaService', 'turmaMultiSeriadaService', '$q'];

function turmaSerieService(conexaoService,conversorService, turmaMultiEtapaService, turmaMultiSeriadaService, $q ){
		var service = {
				modalidadeApontamentoTurma:modalidadeApontamentoTurma,
				cursosDaTurma:cursosDaTurma
			};

			return service;

		function cursosDaTurma(idTurma){
			return conexaoService.executar(cursosDaTurmaQuery,[idTurma])
			.then(function(response){
				return conversorService.toArray(response.rows)
			})
		}

		function modalidadeApontamentoTurma(idTurma){
			return conexaoService.executar(modalidadeApontamentoTurmaQuery,[idTurma])
			.then(function(response){
				if(response.rows.length > 0){
					return conversorService.toObject(response.rows)
				}
				return
			});
		}
	}

	var cursosDaTurmaQuery =
		"select \r\n" +
		"c.id_curso\r\n" +
		"from turma_serie ts\r\n" +
		"inner join serie s on s.id_serie = ts.id_serie\r\n" +
		"inner join curso c on c.id_curso = s.id_curso\r\n" +
		"where ts.id_turma = ?";

	var modalidadeApontamentoTurmaQuery =
		"select\r\n" +
		"t.id_turma, \r\n" +
		"t.flg_frequencia_diaria,\r\n" +
		"s.flg_apon_falta_globalizado\r\n" +
		"from turma t \r\n" +
		"inner join turma_serie ts on ts.id_turma = t.id_turma\r\n" +
		"inner join serie s on s.id_serie = ts.id_serie\r\n" +
		"where t.id_turma = ?";
})();
