(function(){
	angular.module('diario.turmaMultiSeriadaService',[])
		.factory('turmaMultiSeriadaService',turmaMultiSeriadaService);
	
	turmaMultiSeriadaService.$inject = ['conexaoService', 'conversorService'];
	
	function turmaMultiSeriadaService(conexaoService, conversorService){
		var service = {
				isTurmaMultiSeriada:isTurmaMultiSeriada
		};
		
		return service;
		
		function isTurmaMultiSeriada(idTurma){
			return conexaoService.executar(isTurmaMultiSeriadaQuery,[idTurma])
			.then(function(response){
				return conversorService.toObject(response.rows).flg_multiseriada
			});
		}
	}
	
	var isTurmaMultiSeriadaQuery =
		"select \n" + 
		"t.flg_multiseriada \n" + 
		"from turma t \n" + 
		"where \n" + 
		"t.id_turma = ?\n" + 
		"";
})();