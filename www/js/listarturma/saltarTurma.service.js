(function(){
	angular.module('diario.saltarTurmaService',[])
		.factory('saltarTurmaService', saltarTurmaService);
	
	saltarTurmaService.$inject = ['conexaoService', 'conversorService']
	
	function saltarTurmaService(conexaoService, conversorService ){
		var service = {
				numeroTurmasUsuario:numeroTurmasUsuario,
				isTurmaNecessario:isTurmaNecessario,
				recuperarTurmaUnica:recuperarTurmaUnica
		};
		return service;
		
		function isTurmaNecessario(numeroTurmaUsuario){
			if(numeroTurmaUsuario === 1){
				return false;
			}else{
				return true;
			}
		}
		
		function numeroTurmasUsuario(idUnidade){
			return conexaoService.executar(contarTurmaUnicaQuery,[idUnidade])
			.then(function(response){
				return conversorService.toObject(response.rows);
			})
		}
		
		function recuperarTurmaUnica(idUnidade){
			return conexaoService.executar(recuperarTurmaUnicaQuery, [idUnidade])
			.then(function(response){
				return conversorService.toObject(response.rows)
			})
		}
	}
	
	var contarTurmaUnicaQuery = 
		"select \n" + 
		"count \n" + 
		"(t.id_turma) as numero_turma \n" + 
		"from turma t\n" + 
		"where t.id_unidade_funcional = ? ";
	
	var recuperarTurmaUnicaQuery = 
		"select \n" + 
		"t.id_turma, \n" +
		" t.nome " + 
		"from turma t \n" + 
		"where t.id_unidade_funcional = ?"
})();