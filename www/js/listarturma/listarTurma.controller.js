(function(){
	angular.module('diario.listarTurmaController',[])
		.controller('listarTurmaController', listarTurmaController);
		                                     
	listarTurmaController.$inject = ['$state',
                                     '$ionicViewSwitcher',
                                     "$ionicSideMenuDelegate",
                                     "listarTurmaService",
                                     "listarUnidadeService",
                                     "$ionicPopup",
                                     "$ionicModal",
                                     "$scope",
                                     "menuDireitoService",
                                     "conversorService",
                                     "$ionicScrollDelegate",
                                     "$q",
                                     "saltoService",
                                     "menuDireitoService",
                                     "saltarComponenteCurricularService"
                                     ];
	
	function listarTurmaController($state, $ionicViewSwitcher, $ionicSideMenuDelegate, listarTurmaService, listarUnidadeService, 
			$ionicPopup, $ionicModal, $scope, menuDireitoService, conversorService, $ionicScrollDelegate, $q, saltoService, menuDireitoService, saltarComponenteCurricularService){
		
		var ctx = this;
		
		// binds do modelo listar turma
		// manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.backView = definirBackViewTurma();
		ctx.isMenuAberto = isMenuAberto;
		ctx.turmas = listarTurmas($state.params.idUnidade);
		ctx.recuperarComponentes = recuperarComponentes;
		ctx.voltar = voltar;
		
		
		
		
		// implementacoes dos binds dos modelos
		// manter ordem alfabetica
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}
		
		function definirBackViewTurma(){
			saltoService.definirBackViewTurma();
			 return saltoService.getBackViewTurma();
		}
		
		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}
		
		function listarTurmas(idUnidadeFuncional){
			 ctx.turmas = listarTurmaService.getTurmas(idUnidadeFuncional)
			 .then(function(response){
				 return ctx.turmas = conversorService.toArray(response.rows);
			 })
		}
		
		function prepararNavegacao(){
			return listarTurmaService.isTurmaRegenciaClasse($state.params.idTurma)
			.then(function(response){
				if(!response){
					return
				}
				if(response === "true"){
					ctx.proximaView = "listarAula";
					menuDireitoService.setComponenteAtual("Regência de classe");
				}
				return saltarComponenteCurricularService.numeroComponentesUsuario($state.params.idUnidade, $state.params.idTurma, response)
			})
			.then(function(response){
				if(!response){
					return
				}
				 isTurmaRegencia = response.numero_componente == 0
				return saltarComponenteCurricularService.islistarComponenteNecessario(response.numero_componente)
			})
			.then(function(isNecessarioListarComponente){
				saltoService.setMostrarComponente(isNecessarioListarComponente);
				if(isNecessarioListarComponente === true){
					return ctx.proximaView = "listarComponenteCurricular";
				}
				if(isTurmaRegencia != null && !isNecessarioListarComponente ){
					ctx.proximaView = "listarAula";
					return saltarComponenteCurricularService.recuperarComponenteUnico($state.params.idUnidade, $state.params.idTurma) 
				}
			})
			.then(function(response){
				if(!response){
					return;
				}
				menuDireitoService.setComponenteAtual(response.descricao);
				$state.params.idComponente = response.id_componente_curricular
				return ;
			})
		}
		
		function recuperarComponentes(idTurma){
			$ionicViewSwitcher.nextDirection('forward');
			$state.params.idTurma = idTurma;
			return listarTurmaService.getTurma(idTurma)
			.then(function(response){
				menuDireitoService.setTurmaAtual(conversorService.toObject(response.rows).nome);
				return prepararNavegacao()
			})
			.then(function(response){
				$state.go(ctx.proximaView,$state.params);	
			})
		}
		
		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
			$ionicScrollDelegate.resize();
		}
		
	}
})();