(function(){
	angular.module('diario.turmaMultiEtapaService',[])
		.factory('turmaMultiEtapaService',turmaMultiEtapaService);
	
	turmaMultiEtapaService.$inject=['conexaoService', 'conversorService'];
	
	function turmaMultiEtapaService(conexaoService, conversorService){
		var service = {
				isTurmaMultiEtapa:isTurmaMultiEtapa
		}
		
		
		function isTurmaMultiEtapa(idTurma){
			return conexaoService.executar(isTurmaMultiEtapaQuery,[idTurma])
			.then(function(response){
				return conversorService.toObject(response.rows).flg_multietapa;
			});
		}
		
		return service;
	}
	
	var isTurmaMultiEtapaQuery = 
		"select \n" + 
		"t.flg_multietapa \n" + 
		"from turma t \n" + 
		"where \n" + 
		"t.id_turma = ?";
})();