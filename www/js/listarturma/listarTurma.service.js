(function(){
	angular.module('diario.listarTurmaService',[])
		.factory('listarTurmaService',listarTurmaService);
	
	listarTurmaService.$inject = ['conexaoService', 'conversorService', 'turmaMultiSeriadaService', 'turmaMultiEtapaService', 'turmaSerieService']
	
	function listarTurmaService(conexaoService, conversorService, turmaMultiSeriadaService, turmaMultiEtapaService, turmaSerieService){
		
		// service disponivel para a aplicacao
		// manter ordem alfabetica
		var service = {
				getTurma:getTurma,
				getTurmas:getTurmas,
				isTurmaMultietapa:isTurmaMultietapa,
				isTurmaMultiSeriada:isTurmaMultiSeriada,
				isTurmaRegenciaClasse:isTurmaRegenciaClasse
		};
		return service;
		
		
		// retorna a turma selecionada em uma promisse
		function getTurma(idTurma){
			return conexaoService.executar(querySelecionarTurma,[idTurma]);
		};
		
		// recupera todas as turmas da unidade
		function getTurmas(id_unidade_funcional){
			return conexaoService.executar(queryListarTurmas,[id_unidade_funcional]);
		}
		
		function isTurmaMultietapa(idTurma){
			return turmaMultiEtapaService.isTurmaMultiEtapa(idTurma)
		}
		
		function isTurmaMultiSeriada(idTurma){
			return turmaMultiSeriadaService.isTurmaMultiSeriada(idTurma);
		}
		
		function isTurmaRegenciaClasse(idTurma){
			return turmaSerieService.modalidadeApontamentoTurma(idTurma)
			.then(function(response){
				return response.flg_apon_falta_globalizado;
			});
		}
	}
		
	queryListarTurmas = 
		"select \n" + 
		"t.id_turma,\n" + 
		"t.nome,\n" + 
		"t.codigo\n" + 
		"from turma t \n" + 
		"inner join unidade_funcional uf on uf.id_unidade_funcional = t.id_unidade_funcional \n" + 
		"where t.id_unidade_funcional = ?";
	
	querySelecionarTurma = 
		"select \n" + 
		"t.nome \n" + 
		"from turma t \n" + 
		"where t.id_turma = ?";
		
})();