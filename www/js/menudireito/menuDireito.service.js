angular.module('diario.menuDireitoService',[]).factory('menuDireitoService',
    ['$ionicHistory',
    "componenteCurricularService",
    'listarUnidadeService',
    'listarTurmaService',
    'listarAulaService',
  function($ionicHistory, componenteCurricularService,listarUnidadeService, listarTurmaService,listarAulaService){
		
		var _componenteAtual = null;
		var _dataAtual = null;
		var _turmaAtual = null;
		var _unidadeAtual = null;
		var service = {
				
				// manter ordem alfabetica
				getComponenteAtual:getComponenteAtual,
				getDataAtual:getDataAtual,
				getTurmaAtual:getTurmaAtual,
				getUnidadeAtual:getUnidadeAtual,
				setComponenteAtual:setComponenteAtual,
				setDataAtual:setDataAtual,
				setTurmaAtual:setTurmaAtual,
				setUnidadeAtual:setUnidadeAtual,
				viewAtual:viewAtual
		};
		
		// manter ordem alfabetica
		function getComponenteAtual(){
			return _componenteAtual;
		}
		
		function getDataAtual(){
			return _dataAtual;
		}
		
		function getTurmaAtual(){
			return _turmaAtual;
		}
		
		function getUnidadeAtual(){
			return _unidadeAtual;
		}
		
		function setComponenteAtual(componente){
			_componenteAtual = componente;
		}
		
		function setDataAtual(data){
			_dataAtual = data;
		}
		
		function setTurmaAtual(turma){
			_turmaAtual = turma;
		}
		
		function setUnidadeAtual(unidade){
			_unidadeAtual = unidade;
		}
		
		function viewAtual(){
			return $ionicHistory.currentView();
		}
		
		return service;
}]);
