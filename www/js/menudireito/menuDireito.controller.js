angular.module('diario.menuDireitoController',[]).controller('menuDireitoController',['$ionicHistory',
    'menuDireitoService',
    '$state',
    '$ionicSideMenuDelegate',
    "componenteCurricularService",
    'listarUnidadeService',
    'listarTurmaService',
    'listarAulaService',
    'loginService',
    'saltoService',
    'sincronizacaoService',

    function($ionicHistory, 
      menuDireitoService, 
      $state,
      $ionicSideMenuDelegate,	
      componenteCurricularService,
      listarUnidadeService,
      listarTurmaService,
      listarAulaService,
      loginService,
      saltoService,
      sincronizacaoService){
      var ctx = this;

      ctx.mostrarOpcaoMenu = function(){
        menuDireitoService.viewAtual();
      };

      ctx.mostrarSelecionarUnidade = function(){
        return menuDireitoService.viewAtual() && 
          menuDireitoService.viewAtual().title !== "Selecione a Unidade" && 
          menuDireitoService.viewAtual().title !== "Diário de Classe" &&  
          saltoService.isMostrarUnidade();
      };

      ctx.mostrarSelecionarTurma = function(){ 
        return menuDireitoService.viewAtual() && 
          menuDireitoService.viewAtual().title !== "Selecione a Unidade" && 
          menuDireitoService.viewAtual().title !== "Selecione a Turma" && 
          menuDireitoService.viewAtual().title !== "Diário de Classe" && 
          saltoService.isMostrarTurma();
      };

      ctx.mostrarSelecionarComponente = function(){
        return menuDireitoService.viewAtual() && 
          menuDireitoService.viewAtual().title !== "Selecione a Unidade" && 
          menuDireitoService.viewAtual().title !== "Selecione a Turma" && 
          menuDireitoService.viewAtual().title !== "Selecione o Componente" && 
          menuDireitoService.viewAtual().title !== "Diário de Classe" && 
          saltoService.isMostrarComponente();
      };

      ctx.mostrarSelecionarData = function(){
        return menuDireitoService.viewAtual() && 
          menuDireitoService.viewAtual().title !== "Diário de Classe" && 
          menuDireitoService.viewAtual().title !== "Selecione a Unidade" && 
          menuDireitoService.viewAtual().title !== "Selecione a Turma" && 
          menuDireitoService.viewAtual().title !== "Selecione o Componente" && 
          menuDireitoService.viewAtual().title !== "Selecione a Data"; 
      };

      ctx.irParaView = function(view){
        $state.go(view,$state.params);
        $ionicSideMenuDelegate.toggleRight();
      };

      ctx.usuario = function(){
        return (loginService.usuario ? loginService.usuario.nomeUsuario : "");
      };

      ctx.unidade = function(){
        return	menuDireitoService.getUnidadeAtual();
      };

      ctx.turma = function(){
        return menuDireitoService.getTurmaAtual();
      } ;

      ctx.componente = function(){
        return menuDireitoService.getComponenteAtual();
      };

      ctx.data = function(){
        return  menuDireitoService.getDataAtual();
      };

      ctx.logoff = function() {
        loginService.logoff();
        ctx.irParaView('login');
      };

      ctx.sincronizar = function() {
        sincronizacaoService.sincronizar();
      };
}]);
