(function(){
	angular.module('diario.focusDirective',[])
		.directive('focusDirective',focusDirective);
	
	function focusDirective(){
		return {
			restrict:"A",
			scope:{
				focusValue:"=focusDirective"
			},
			link:linkFunction
		};
		
		function linkFunction($scope, $element, attr){
			$scope.$watch("focusValue", function(currentValue, previousValue) {
		        if (currentValue === true) {
		          $element[0].focus();
		        } 
		      });
		}
	}
})();