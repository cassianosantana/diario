(function(){
	angular.module('diario.saltoService',[])
		.factory('saltoService',saltoService);
	
	function saltoService(){
		 var service ={
				 definirBackViewComponente:definirBackViewComponente,
				 definirBackViewData:definirBackViewData,
				 definirBackViewTurma:definirBackViewTurma,
				 isMostrarComponente:isMostrarComponente,
				 isMostrarTurma:isMostrarTurma,
				 isMostrarUnidade:isMostrarUnidade,
				 getBackViewComponente:getBackViewComponente,
				 getBackViewData:getBackViewData,
				 getBackViewTurma:getBackViewTurma,
				 getBackViewUnidade:getBackViewUnidade,
				 getForwardViewComponente:getForwardViewComponente,
				 getForwardViewModulo:getForwardViewModulo,
				 getForwardViewTurma:getForwardViewTurma,
				 getForwardViewUnidade:getForwardViewUnidade,
				 setBackViewComponente:setBackViewComponente,
				 setBackViewData:setBackViewData,
				 setBackViewTurma:setBackViewTurma,
				 setBackViewUnidade:setBackViewUnidade,
				 setMostrarComponente:setMostrarComponente,
				 setMostrarTurma:setMostrarTurma,
				 setMostrarUnidade:setMostrarUnidade
		 };
		 return service;
		 
		 function definirBackViewComponente(){
			 if(_isMostrarTurma){
				 _backViewComponente = "listarTurma";
				 return;
			 }
			 
			 if(_isMostrarUnidade){
				 _backViewComponente = "listarUnidade"
				return; 	 
			 }
			 _backViewComponente = "listarModulo";
		 }
		 
		 function definirBackViewData(){
			 if(_isMostrarComponente){
				 _backViewData = "listarComponenteCurricular";
				 return;
			 }
			 
			 if(_isMostrarTurma){
				 _backViewData = "listarTurma";
				return; 	 
			 }
			 
			 if(_isMostrarUnidade){
				 _backViewData = "listarUnidade";
				 return;
			 }
			 _backViewData = "listarModulo";
		 }
		 
		 function definirBackViewTurma(){
			 if(_isMostrarUnidade){
				 _backViewTurma = "listarUnidade";
				return;	 
			 }
			 _backViewTurma = "listarModulo";
		 }
		 
		 function isMostrarComponente(){
			 return _isMostrarComponente;
		 }
		 
		 function isMostrarTurma(){
			 return _isMostrarTurma;
		 }
		 
		 function isMostrarUnidade(){
			 return _isMostrarUnidade;
		 }
		 
		 function getBackViewComponente(){
			 return _backViewComponente;
		 }
		 
		 function getBackViewData(){
			 return _backViewData;
		 }
		 
		 function getBackViewTurma(){
			 return _backViewTurma;
		 }
		 
		 function getBackViewUnidade(){
			 return _backViewUnidade;
		 }
		 
		 function getForwardViewComponente(){
			 return _forwardViewComponente;
		 }
		 
		 function getForwardViewModulo(){
			 return _forwardViewModulo;
		 }
		 
		 function getForwardViewTurma(){
			 return _forwardViewTurma;
		 }
		 
		 function getForwardViewUnidade(){
			 return _forwardViewUnidade;
		 }
		 
		 function setBackViewComponente(backView){
			 _backViewComponente = backView;
		 }
		 
		 function setBackViewData(backView){
			 _backViewData = backView;
		 }
		 
		 function setBackViewTurma(backView){
			 _backViewTurma = backView;
		 }
		 
		 function setBackViewUnidade(backView){
			 _backViewUnidade = _backViewUnidade;
		 }
		 
		 function setMostrarComponente(isNecessario){
			 _isMostrarComponente = isNecessario;
		 }
		 
		 function setMostrarTurma(isNecessario){
			 _isMostrarTurma = isNecessario;
		 }
		 
		 function setMostrarUnidade(isNecessario){
			 _isMostrarUnidade = isNecessario;
		 }
	}
	
	/// opções default de back view 
	var _backViewComponente = "listarTurma";
	var _backViewData = "listarComponenteCurricular";
	var _backViewTurma = "listarUnidade";
	var _backViewUnidade = "listarModulo";
	var _isMostrarComponente = true;
	var _isMostrarUnidade = true;
	var _isMostrarTurma = true;
	var _forwardViewComponente="listarAula";
	var _forwardViewModulo ="listarUnidade";
	var _forwardViewTurma="listarComponenteCurricular";
	var _forwardViewUnidade = "listarTurma";
})();