(function() {
  angular.module('diario.conexaoService', []).factory('conexaoService', conexaoService);

  conexaoService.$inject = ['$cordovaSQLite', '$window'];

  function conexaoService($cordovaSQLite, $window) {
    var service = {};

    // Conexao com o banco de dados do usuario
    service.db = null;

    service.executar = function(query, parametros) {
      parametros = parametros || [];
      return $cordovaSQLite.execute(service.db, query, parametros).then(function(response) {
        console.debug("Query:" + query + ", parametros: " + parametros);
        return response;
      }, function(error) {
        console.debug(error);
        console.debug("Erro na query:" + query + "parametros" + parametros);
        return error;
      });
    };

    service.recuperarBanco = function(idUsuario) {
      if ($window.sqlitePlugin) {
        service.db = $cordovaSQLite.openDB({
          name : "diario_" + idUsuario + ".db"
        });
      } else {
        service.db = window.openDatabase("diario_" + idUsuario + ".db", "1.0",
            "diario_" + idUsuario + ".db", -1);
      }
    };

    return service;
  }
})();
