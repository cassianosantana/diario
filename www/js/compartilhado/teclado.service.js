(function(){
	angular.module('diario.tecladoService',[])
		.factory('tecladoService',tecladoService);
	
	function tecladoService(){
		var service = {
				isTeclaEnterPressionada:isTeclaEnterPressionada
		};
		
		return service;
		
		function isTeclaEnterPressionada(keyCode){
			return keyCode == 13
		}
	}
})();