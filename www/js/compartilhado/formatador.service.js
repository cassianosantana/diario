(function(){
	angular.module('diario.formatadorService',[]).factory('formatadorService', formatadorService);
	
	formatadorService. $inject = ['$filter'];
	
	function formatadorService($filter){
    var service = {};
		
		service.formatatarDataSql = function(data){
			return $filter('date')(data,'yyyy-MM-dd');
    };
		
		service.formatatarHoraSql = function(hora){
      if(angular.isDate(hora)) {
        return $filter('date')(hora,'hh:mm:ss');
      } else {
        if((hora.match(/:/g) || []).length == 1) {
          return hora + ":00";
        } else {
          return hora;
        }
      }
    };
		
		return service;
	}
})();
