(function(){
	angular.module('diario.popupConcluidoDirective',[])
		.directive('popupConcluidoDirective',['$state', 'saltoService',function($state, saltoService){
			return{
				restrict:"E",
				templateUrl:"js/compartilhado/directives/templates/popupConcluidoDirective.html",
				link:function($scope, $element, $attr){
					$scope.fechar = function(view){
						$scope.modal.hide()
						$state.go(view,$state.params)
					}
					$scope.mostrarTurma = saltoService.isMostrarTurma()
				}	
			}
		}]);
})();