(function(){
	angular.module('diario.conversorService',[]).factory('conversorService',conversorService);
	
	function conversorService(){
    var service = {};
		
		service.toArray = function(rows) {
			var array = [];
			for(var i=0; i< rows.length; i++){
				array.push(rows.item(i));
			}
			return array;
    };
		
		service.toObject = function(rows){
			if(rows.length == 0){
				return null;
			}
			return rows.item(0);
    };

		return service;
	}	
})();
