(function(){
	angular.module('diario.detalharEstadoController',[])
		.controller('detalharEstadoController',['$state',
		                                        'listarUnidadeService',
		                                        'listarTurmaService',
		                                        'componenteCurricularService',
		                                        'listarAulaService',
		                                        'modalService',
		                                        detalharEstadoController])

	function detalharEstadoController($state,listarUnidadeService, listarTurmaService, componenteCurricularService, listarAulaService, modalService){
		var ctx = this;
		
		ctx.modal = modalService.getModal();
		
		ctx.fecharModal = function(){
			ctx.modal.hide();
		}
		
		
		
		componenteCurricularService.getComponente($state.params.idComponente).then(function(data){
			debugger
			ctx.componente = data.nome;
		})
		
		 listarUnidadeService.getUnidade($state.params.idUnidade).then(function(data){
			 debugger
			 ctx.unidade = data.nome;
		});	
		 
		listarTurmaService.getTurma($state.params.idTurma).then(function(data){
			debugger;
			ctx.turma = data.nome;
		});
		
		debugger
		ctx.data= listarAulaService.getDataSelecionada();
		
		
			 
	}
})();