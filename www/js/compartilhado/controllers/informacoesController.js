(function(){
	angular.module('diario.informacoesController',[])
		.controller('informacoesController',["$ionicModal",
		                                     "$scope",
		                                     "modalService",
		                                     informacoesController])
	function informacoesController($ionicModal, $scope, modalService){
		var ctx = this;
		
		$ionicModal.fromTemplateUrl('js/compartilhado/modaltemplates/modalDetalharTemplate.html', {
		    scope: $scope,
		    animation: 'slide-in-up'
		  }).then(function(modal) {
			  ctx.modal= modal;
			  $scope.modal = modal;
			  modalService.setModal(modal);
		  });
		
		ctx.detalhar = function(){
			ctx.modal.show();
		}
	}
})();