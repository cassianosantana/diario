(function() {
  angular.module('diario.sincronizacaoService', []).factory('sincronizacaoService', sincronizacaoService);

  sincronizacaoService.$inject = [
    '$q',
    'conexaoService',
    'loginService',
    'faltaAlunoGlobalizadaDiarioService',
    'faltaAlunoDisciplinaDiarioService'];

  function sincronizacaoService(
      $q,
      conexaoService,
      loginService,
      faltaAlunoGlobalizadaDiarioService,
      faltaAlunoDisciplinaDiarioService) {
    var service = {};

    service.registrar = function(tipo, id, operacao, conteudo) {
      // Converter id e conteudo para JSON
      if(!angular.isString(id)) {
        id = angular.toJson(id);
      }
      if(conteudo != null && !angular.isString(conteudo)) {
        conteudo = angular.toJson(conteudo);
      }
      var ts = new Date();

      // Verificar se a chave ja existe
      return conexaoService.executar(
          "select id " +
          "from pacote_sincronizacao " +
          "where tipo = ? and id = ? and data_sincronizacao is null", 
          [tipo, id]).then(function(r) {
        if(r.rows.length > 0) {
          return conexaoService.executar("update pacote_sincronizacao set operacao = ?, " +
              "conteudo = ?, data_criacao = ? where tipo = ? and id = ?",
              [operacao, conteudo, ts, tipo, id]);
        } else {
          return conexaoService.executar("insert into pacote_sincronizacao (tipo, id, " +
                "operacao, conteudo, data_criacao) values (?, ?, ?, ?, ?)",
              [tipo, id, operacao, conteudo, ts]);
        }
      });
    };

    service.sincronizar = function() {
      service.enviarPacotes();
    }

    service.enviarPacotes = function() {
      var pacotes = null;
      service.gerarPacotes().then(function(resultado) {
        pacotes = angular.toJson(resultado);
        return loginService.iniciarSessao();
      }).then(function() {
        return $http.post(URL_BACKEND + "/ws/diarioclasse/recepcao_pacotes", {
          "pacotes": pacotes
        });
      });
    };

    var funcoes = {
      'frequencia_diaria_regencia': service.gerarPacoteFrequenciaDiariaRegencia,
      'frequencia_diaria_componente': service.gerarPacoteFrequenciaDiariaComponente
    };
    service.gerarPacotes = function() {
      var promises = [];
      var pacotes = [];
      return conexaoService.executar(
          "select tipo, " +
          " id, " +
          " operacao, " +
          " conteudo, " +
          " data_criacao " +
          "from pacote_sincronizacao " +
          "where data_sincronizacao is null " +
          "order by data_criacao").then(function(lista) {
        angular.forEach(lista, function(item) {
          var id = angular.fromJson(item["id"]);
          var conteudo = item["conteudo"] == null ? null : angular.fromJson(item["conteudo"]);
          lista.push(funcoes[lista["id"]](id, conteudo).then(function(pacote) {
            id["conteudo"] = pacote;
            pacotes.push(id);
          }));
        });
        return $q.all(promises).then(function() {
          return pacotes;
        });
      });
    };

    service.gerarPacoteFrequenciaDiariaRegencia = function(id, conteudo) {
      return faltaAlunoGlobalizadaDiarioService.pacoteSincronizacao(id.idTurma, id.data);
    };

    service.gerarPacoteFrequenciaDiariaComponente = function(id, conteudo) {
      return faltaAlunoDisciplinaDiarioService.pacoteSincronizacao(id.idTurma, id.idComponente, id.data);
    };

    return service;
  }
})();
