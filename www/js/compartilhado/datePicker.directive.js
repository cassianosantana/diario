"use strict";
//https://github.com/rajeshwarpatlolla/ionic-datepicker base para referencia
(function(){
	angular.module('diario.datePickerDirective',[])
		.directive('datePickerDirective',datePickerDirective);

	datePickerDirective.$inject = ['$ionicPopup',
	'datePickerService','cursoCalendarioService', 'escolaCalendarioService', 'calendarioDataService', 'calendarioService', 'turmaSerieService','$q', 'listarAulaService'];

	function datePickerDirective($ionicPopup,
		datePickerService, cursoCalendarioService, escolaCalendarioService,
			calendarioDataService, calendarioService, turmaSerieService, $q, listarAulaService){

		var directive = {
				restrict:'AE',
				replace:true,
				scope:{
					ipDate: '=',
					disablePreviousDates: '=',
					disableFutureDates:'=',
					callback:'=',
					title:'=',
					datePickerObject:'=',
					dadosTurma:'='

				},
				link:linkFunction
		};
		return directive

		function linkFunction(scope, element, attrs){

			scope.datePickerTitle = scope.datePickerObject.titleLabel || 'Select Date';

		      var monthsList = datePickerService.monthsList;
		      scope.monthsList = monthsList;
		      scope.yearsList = datePickerService.yearsList;

		      scope.currentMonth = '';
		      scope.currentYear = '';

		      if (!scope.ipDate) {
		        scope.ipDate = new Date();
		      }

		      scope.previousDayEpoch = (+(new Date()) - 86400000);
		      scope.nextDayEpoch = (+(new Date()));

		      var currentDate = angular.copy(scope.datePickerObject.inputDate);
		      currentDate.setHours(0);
		      currentDate.setMinutes(0);
		      currentDate.setSeconds(0);
		      currentDate.setMilliseconds(0);

		      scope.selctedDateString = currentDate.toString();
		      scope.weekNames = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];
		      scope.today = {};

		      var tempTodayObj = new Date();
		      var tempToday = new Date(tempTodayObj.getFullYear(), tempTodayObj.getMonth(), tempTodayObj.getDate());

		      scope.today = {
		        dateObj: tempTodayObj,
		        date: tempToday.getDate(),
		        month: tempToday.getMonth(),
		        year: tempToday.getFullYear(),
		        day: tempToday.getDay(),
		        dateString: tempToday.toString(),
		        epochLocal: tempToday.getTime(),
		        epochUTC: (tempToday.getTime() + (tempToday.getTimezoneOffset() * 60 * 1000))
		      };

		      var refreshDateList = function (current_date) {
						scope.data_calendario_invalida = false;
						scope.erroCalendario = ""
		        current_date.setHours(0);
		        current_date.setMinutes(0);
		        current_date.setSeconds(0);
		        current_date.setMilliseconds(0);

		        scope.selctedDateString = (new Date(current_date)).toString();
		        currentDate = angular.copy(current_date);

		        var firstDay = new Date(current_date.getFullYear(), current_date.getMonth(), 1).getDate();
		        var lastDay = new Date(current_date.getFullYear(), current_date.getMonth() + 1, 0).getDate();

		        scope.dayList = [];

		        for (var i = firstDay; i <= lastDay; i++) {
		          var tempDate = new Date(current_date.getFullYear(), current_date.getMonth(), i);
		          scope.dayList.push({
		            date: tempDate.getDate(),
		            month: tempDate.getMonth(),
		            year: tempDate.getFullYear(),
		            day: tempDate.getDay(),
		            dateString: tempDate.toString(),
		            epochLocal: tempDate.getTime(),
		            epochUTC: (tempDate.getTime() + (tempDate.getTimezoneOffset() * 60 * 1000))
		          });
		        }

		        var firstDay = scope.dayList[0].day;

		        scope.currentMonthFirstDayEpoch = scope.dayList[0].epochLocal;
		        scope.currentMonthLastDayEpoch = scope.dayList[scope.dayList.length - 1].epochLocal;

		        for (var j = 0; j < firstDay; j++) {
		          scope.dayList.unshift({});
		        }

		        scope.rows = [];
		        scope.cols = [];

		        scope.currentMonth = monthsList[current_date.getMonth()];
		        scope.currentYear = current_date.getFullYear();
		        scope.currentMonthSelected = scope.currentMonth;
		        scope.currentYearSelected = scope.currentYear;

		        scope.numColumns = 7;
		        scope.rows.length = 6;
		        scope.cols.length = scope.numColumns;
		      };

		      scope.monthChanged = function (month) {
		        var monthNumber = scope.monthsList.indexOf(month);
		        currentDate.setMonth(monthNumber);
		        refreshDateList(currentDate)
		      };

		      scope.yearChanged = function (year) {
		        currentDate.setFullYear(year);
		        refreshDateList(currentDate)
		      };

		      scope.prevMonth = function () {
		        if (currentDate.getMonth() === 1) {
		          currentDate.setFullYear(currentDate.getFullYear());
		        }
		        currentDate.setMonth(currentDate.getMonth() - 1);

		        scope.currentMonth = monthsList[currentDate.getMonth()];
		        scope.currentYear = currentDate.getFullYear();

		        refreshDateList(currentDate)
		      };

		      scope.nextMonth = function () {
		        if (currentDate.getMonth() === 11) {
		          currentDate.setFullYear(currentDate.getFullYear());
		        }
		        currentDate.setMonth(currentDate.getMonth() + 1);
		        scope.currentMonth = monthsList[currentDate.getMonth()];
		        scope.currentYear = currentDate.getFullYear();
		        refreshDateList(currentDate)
		      };

		      scope.date_selection = {selected: false, selectedDate: '', submitted: false};

		      scope.dateSelected = function (date) {

		        scope.selctedDateString = date.dateString;
		        scope.date_selection.selected = true;
		        scope.date_selection.selectedDate = new Date(date.dateString);
						return scope.validarData(scope.date_selection.selectedDate, scope.dadosTurma.id_unidade, scope.dadosTurma.id_turma, scope.dadosTurma.id_componente)
						.then(function(response){
						scope.data_calendario_invalida = false;
						scope.isDataValidaCalendario = true;
						},function(error){
							scope.data_calendario_invalida = true;
							scope.erroCalendario = error.message;
							scope.isDataValidaCalendario = false;
						})
		      };

					scope.validarData = function(dataSelecionada, idUnidade, idTurma, idComponente){
						return scope.recuperarCursosUnidade(idUnidade, idTurma,idComponente, dataSelecionada)
						.then(scope.recuperarIdCalendario)
						.then(scope.validarDataIdCalendario)
						.then(function(response){

						})
					}

					scope.recuperarCursosUnidade = function(idUnidade, idTurma, idComponente, dataSelecionada){
						return turmaSerieService.cursosDaTurma(idTurma)
						.then(function(cursos){
							return {
								cursos:cursos,
								idUnidade:idUnidade,
								idComponente:idComponente,
								idTurma:idTurma,
								dataSelecionada:dataSelecionada
							};
						});
					}

					scope.recuperarIdCalendario = function(cursosUnidade){
						var promessas = [];
						angular.forEach(cursosUnidade.cursos,function(curso){
							promessas.push(cursoCalendarioService.getIdCursoCalendario(cursosUnidade.idUnidade,curso.idCurso, cursosUnidade.dataSelecionada.getFullYear()))
						})
						promessas.push(escolaCalendarioService.getIdEscolaCalendario(cursosUnidade.idTurma))
						return $q.all(promessas)
						.then(function(response){
							var idCalendarios  = []
							angular.forEach(response,function(item){
								if(item.id_calendario_curso != null || item.id_calendario != null){
										idCalendarios.push(item)
								}
							})
							return {
								idCalendarios:idCalendarios,
								idUnidade:cursosUnidade.idUnidade,
								idComponente:cursosUnidade.idComponente,
								idTurma:cursosUnidade.idTurma,
								dataSelecionada:cursosUnidade.dataSelecionada
							};
						});
					};
					scope.validarDataIdCalendario = function(informacaoCalendarioTurma){
						if(informacaoCalendarioTurma.idCalendarios.length == 1){
							var idCalendario = informacaoCalendarioTurma.idCalendarios[0].id_calendario;
							return listarAulaService.isDataValidaTurmaComponente(informacaoCalendarioTurma.dataSelecionada, idCalendario, informacaoCalendarioTurma.idTurma, informacaoCalendarioTurma.idComponente)
						}
					}


		      element.on("click", function () {
		        if (!scope.ipDate) {

		          var defaultDate = new Date();
		          refreshDateList(defaultDate);
		        } else {
		          refreshDateList(angular.copy(scope.ipDate));
		        }

		      var popupCassio =  $ionicPopup.show({
		          templateUrl: 'js/compartilhado/modaltemplates/date-picker-modal.html',
		          title: scope.datePickerTitle,
		          subTitle: '',
		          scope: scope,
		          buttons: [
		            {
		              text: scope.datePickerObject.closeLabel,
		              onTap: function (e) {
		            	  popupCassio.close();
		              }
		            },
		            {
		              text: scope.datePickerObject.todayLabel,
		              onTap: function (e) {

		                var today = new Date();
		                today.setHours(0);
		                today.setMinutes(0);
		                today.setSeconds(0);
		                today.setMilliseconds(0);

		                var tempEpoch = new Date(today.getFullYear(), today.getMonth(), today.getDate());
		                var todayObj = {
		                  date: today.getDate(),
		                  month: today.getMonth(),
		                  year: today.getFullYear(),
		                  day: today.getDay(),
		                  dateString: today.toString(),
		                  epochLocal: tempEpoch.getTime(),
		                  epochUTC: (tempEpoch.getTime() + (tempEpoch.getTimezoneOffset() * 60 * 1000))
		                };

		                scope.selctedDateString = todayObj.dateString;
		                scope.date_selection.selected = true;
		                scope.date_selection.selectedDate = new Date(todayObj.dateString);
		                refreshDateList(new Date());
		                e.preventDefault();

		              }
		            },
		            {
		              text: scope.datePickerObject.setLabel,
		              type: 'button-positive',
		              onTap: function (e) {
										scope.date_selection.submitted = true;
		                if (scope.date_selection.selected === true && scope.isDataValidaCalendario === true){
											scope.ipDate = angular.copy(scope.date_selection.selectedDate);
											scope.datePickerObject.callback(scope.ipDate);
		                } else {
		                  e.preventDefault();
		                }

		              }
		            }
		          ]
		        })

		      })
		}
	}
})();
