(function(){
	angular.module('diario.conexaoRedeService',[])
		.factory('conexaoRedeService',conexaoRedeService);
	
	conexaoRedeService.$inject = ['$cordovaNetwork'];
	
	function conexaoRedeService($cordovaNetwork){
		var service = {
				temConexao:temConexao
		}
		
		return service;
		
		function temConexao(){
			 return $cordovaNetwork.isOnline();  
		}
		
		
	}
	
})();