(function(){
	angular.module('diario.saltarComponenteCurricularService',[])
		.factory('saltarComponenteCurricularService',saltarComponenteCurricular);
	
	saltarComponenteCurricular.$inject = ['conexaoService', 'conversorService'];
	
	function saltarComponenteCurricular(conexaoService, conversorService){
	
		var service = {
				numeroComponentesUsuario:numeroComponentesUsuario,
				islistarComponenteNecessario:islistarComponenteNecessario,
				recuperarComponenteUnico:recuperarComponenteUnico
		};
		
		return service;
	
		function islistarComponenteNecessario(numeroComponenteUsuario){
			if( numeroComponenteUsuario <= 1){
				return false
			}else{
				return true
			}
		}
		
		function numeroComponentesUsuario(idUnidade, idTurma, flgTurmaRegencia){
			if(flgTurmaRegencia === "true"){
				return{
					numero_componente:0
				};
			}
			return conexaoService.executar(componenteCurricularUnicoQuery, [idTurma, idUnidade])
			.then(function(response){
				return conversorService.toObject(response.rows)
			})
		}
		
		function recuperarComponenteUnico(idUnidade, idTurma){
			return conexaoService.executar(recuperarComponenteUnicoQuery,[idUnidade, idTurma])
			.then(function(response){
				return conversorService.toObject(response.rows);
			})
		}
	}
	
	var componenteCurricularUnicoQuery = 
		"\n" + 
		"select \n" + 
		"count (cc.id_componente_curricular)  as numero_componente \n" + 
		"from componente_curricular cc \n" + 
		"	inner join turma t on t.id_grade_curricular = cc.id_grade_curricular\n" +
		"   inner join turma_componente tc on tc.id_componente_curricular = cc.id_componente_curricular and tc.id_turma = t.id_turma \n " + 
		"where \n" + 
		"t.id_turma = ? \n" + 
		"and t.id_unidade_funcional = ?";
	
	var recuperarComponenteUnicoQuery = 
		"select \n" + 
		"cc.id_componente_curricular,\n" + 
		"d.descricao \n" + 
		"from componente_curricular cc \n" + 
		"inner join disciplina d \n" + 
		"on cc.id_disciplina = d.id_disciplina\n" + 
		"inner join turma t on cc.id_grade_curricular = t.id_grade_curricular\n" + 
		"where \n" + 
		"t.id_unidade_funcional = ?\n" + 
		"and t.id_turma = ?\n" + 
		"";
})();