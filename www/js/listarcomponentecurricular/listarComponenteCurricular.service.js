(function(){
	angular.module('diario.listarcomponenteCurricularService',[])
		.factory('componenteCurricularService',componenteCurricularService);
	
	componenteCurricularService.$inject = ['conexaoService', 'conversorService'];
	
	function componenteCurricularService(conexaoService, conversorService){
		
		
		
		var service = {
				getComponente:getComponente,
				getComponentes:getComponentes,
				getComponenteCurricularGrade:getComponenteCurricularGrade,
				getDisciplinaComponenteCurricular:getDisciplinaComponenteCurricular,
				getIdDescricaoDisciplina:getIdDescricaoDisciplina,
				getGradeCurricular:getGradeCurricular
		};
		return service;
		
		// implementacoes dos metodos do service 
		// manter ordem alfabetica
		function getComponente(idComponenteCurricular){	
			return conexaoService.executar(querySelecionar,[idComponenteCurricular]);
		}
		
		// retorna os componentes da turma que o professor da aula
		function getComponentes(idTurma, idServidorCargoBase){
			
			return conexaoService.executar(listarQuery,[idTurma]);
		}
		
		function getComponenteCurricularGrade(idGradeCurricular){
			return conexaoService.executar(recuperarComponenteGrade,[idGradeCurricular])
				.then(function(response){
					console.debug(conversorService.toArray(response.rows));
					return conversorService.toArray(response.rows)
				})
		}
		
		function getIdDescricaoDisciplina(idDisciplina){
			return conexaoService.executar(queryRecuperarIdDescricaoDisciplina,[idDisciplina])
				.then(function(response){
					return conversorService.toArray(response.rows);
				});
		}
		
		function getDisciplinaComponenteCurricular(idComponenteCurricular){
			return conexaoService.executar(queryRecuperarDisciplina,[idComponenteCurricular])
				.then(function(response){
					return conversorService.toObject(response.rows)
				})
		}
		
		function getGradeCurricular(idTurma){
			return conexaoService.executar(queryRecuperarGradeCurricular,[idTurma])
				.then(function(response){
					return conversorService.toObject(response.rows);
				});
		}
	}
	
	var listarQuery = 	
		"select \r\n" + 
		"  cc.id_componente_curricular, \r\n" + 
		"  d.descricao\r\n" + 
		"  from disciplina d \r\n" + 
		"	inner join componente_curricular cc on cc.id_disciplina = d.id_disciplina  \r\n" + 
		"	inner join grade_curricular gc on gc.id_grade_curricular = cc.id_grade_curricular  \r\n" + 
		"	inner join turma t on t.id_grade_curricular = gc.id_grade_curricular\r\n" + 
		"	inner join turma_componente tc on tc.id_componente_curricular = cc.id_componente_curricular and tc.id_turma = t.id_turma\r\n" + 
		"	inner join turma_componente_servidor tcs on  tcs.id_turma_componente = tc.id_turma_componente\r\n" + 
		" where t.id_turma =   ?\r\n" + 
    " order by d.descricao";

		var querySelecionar = 
			" select \n" + 
			" d.descricao \n" + 
			" from disciplina d \n" +
			" inner join componente_curricular cc on cc.id_disciplina = d.id_disciplina \n" + 
			" where cc.id_componente_curricular  = ? ";
		
		var recuperarComponenteGrade = 
			"select " +
			"id_componente_curricular " +
			"from " +
			"componente_curricular cc " +
      "where cc.id_grade_curricular = ?";
			
		var queryRecuperarDisciplina = 
			"select \r\n " + 
			"cc.id_disciplina \r\n " + 
			"from \r\n" + 
			"componente_curricular cc\r\n " + 
      "where cc.id_componente_curricular = ? ";
			
		var queryRecuperarGradeCurricular = 
			"select" +
			"t.id_grade_curricular " +
			"from turma t " +
			"where t.id_turma = ?";
		
		var queryRecuperarIdDescricaoDisciplina = 
			"select" +
			" d.id_disciplina," +
			"d.descricao " +
			"from disciplina d " +
      "where d.id_disciplina = ?";
})();
