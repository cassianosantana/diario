(function(){
	angular.module('diario.listarComponenteCurricularController',[])
		.controller('listarComponenteCurricularController',listarComponenteCurricularController);
	
	listarComponenteCurricularController.$inject=['$state',
                                                  '$ionicViewSwitcher',
                                                  '$ionicSideMenuDelegate',
                                                  '$ionicPopup',
                                                  'componenteCurricularService',
                                                  'listarUnidadeService',
                                                  'listarTurmaService',
                                                  'menuDireitoService',
                                                  'conversorService',
                                                  'loginService',
                                                  'saltoService'];
	
	function listarComponenteCurricularController($state, $ionicViewSwitcher, $ionicSideMenuDelegate, $ionicPopup, 
			componenteCurricularService, listarUnidadeService,  listarTurmaService, 
			menuDireitoService, conversorService, loginService, saltoService){
		
		var ctx = this;
		
		// binds do modelo de listar turma 
		// manter ordem alfabetica
		ctx.abrirMenu = abrirMenu;
		ctx.backView = definirViewAnterior();
		ctx.componentes = listarComponentes($state.params.idTurma, loginService.usuario.idServidorCargoBaseLogado);
		ctx.isMenuAberto = isMenuAberto;
		ctx.recuperarModulos = recuperarModulos;
		ctx.voltar = voltar;
		
		// implementacoes dos binds do modelo listar turma 
		// manter ordem alfabetica 
		function abrirMenu(){
			$ionicSideMenuDelegate.toggleRight();
		}
		
		function definirViewAnterior(){
			saltoService.definirBackViewComponente();
			return saltoService.getBackViewComponente();
		}
		
		function isMenuAberto(){
			return $ionicSideMenuDelegate.isOpenRight();
		}
		
		function listarComponentes(idTurma, idServidorCargoBase){
			componenteCurricularService.getComponentes(idTurma, idServidorCargoBase)
			.then(function(response){
				return ctx.componentes = conversorService.toArray(response.rows);
      });
		}
		
		function recuperarModulos(idComponenteCurricular){
			componenteCurricularService.getComponente(idComponenteCurricular)
			.then(function(response){
				menuDireitoService.setComponenteAtual(conversorService.toObject(response.rows).descricao);
				$ionicViewSwitcher.nextDirection('forward');
				$state.go("listarAula",{
					"idUnidade":$state.params.idUnidade,
					"idTurma":$state.params.idTurma,
					"idComponente":idComponenteCurricular
				});
			});
		}
		
		function voltar(state){
			$ionicViewSwitcher.nextDirection('back');
			$state.go(state,$state.params);
		}
	}
})();
